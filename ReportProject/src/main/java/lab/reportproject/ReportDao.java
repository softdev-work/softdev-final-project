/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab.reportproject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author peakk
 */
public class ReportDao implements Dao<Product> {

    @Override
    public Product get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<Product> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Product save(Product obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Product update(Product obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(Product obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<Product> getAll(String where, String order) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public List<Report> getProductByAmount(int limit) {
    ArrayList<Report> list = new ArrayList();
    String sql = "SELECT pd.PD_CODE, pd.PD_NAME as ProductName, RCD.RC_D_PD_AMOUNT as PRODUCTAMOUNT " +
            "FROM RECIEPTDETAILS RCD " +
            "INNER JOIN Product pd ON pd.PD_CODE = RCD.RC_D_CODE " +
            "INNER JOIN RECIEPT RCP ON RCP.RECE_CODE = RCD.RC_D_CODE AND strftime('%Y',RECE_DATE)='2023' "+
            "GROUP BY RCD.PD_CODE " +
            "ORDER BY RC_D_PD_AMOUNT DESC " +
            "LIMIT ?";
    Connection conn = DatabaseHelper.getConnect();
    try {
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1,limit);
        ResultSet rs = stmt.executeQuery();
        
        while (rs.next()) {
            Report obj = Report.fromRS(rs);
            list.add(obj);
        }
    } catch (SQLException ex) {
        System.out.println(ex.getMessage());
    }
    return list;
}
    public List<Report> getProductByAmount(String begin,String end,int limit) {
    ArrayList<Report> list = new ArrayList();
    String sql = "SELECT pd.PD_CODE, pd.PD_NAME as ProductName, RCD.RC_D_PD_AMOUNT as PRODUCTAMOUNT " +
            "FROM RECIEPTDETAILS RCD " +
            "INNER JOIN Product pd ON pd.PD_CODE = RCD.RC_D_CODE " +
            "INNER JOIN RECIEPT RCP ON RCP.RECE_CODE = RCD.RC_D_CODE AND RECE_DATE BETWEEN ? AND ?"+
            "GROUP BY RCD.PD_CODE " +
            "ORDER BY RC_D_PD_AMOUNT DESC " +
            "LIMIT ?";
    Connection conn = DatabaseHelper.getConnect();
    try {
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1,begin);
        stmt.setString(2,end);
        stmt.setInt(3,limit);
        ResultSet rs = stmt.executeQuery();
        
        while (rs.next()) {
            Report obj = Report.fromRS(rs);
            list.add(obj);
        }
    } catch (SQLException ex) {
        System.out.println(ex.getMessage());
    }
    return list;
}
}