/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package GUI;

import Service.IngredientService;
import com.mycompany.d.coffee.Dao.IngredientDao;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import model.Ingredient;

public class IngredientPanel extends javax.swing.JPanel {

    private final IngredientService ingredientService;
    private List<Ingredient> list;
    private Ingredient editedIngredient;
    private Ingredient editedCheckStock;
    private int isSearch;
    private int SearchInd;

    public IngredientPanel() {
        initComponents();
        ingredientService = new IngredientService();
        list = ingredientService.getIngredient();
        tblIngredient = new JTable(new AbstractTableModel() {
            String[] columnnames = {"ING_CODE","ING_NAME", "ING_PRICE", "ING_TYPE", "ING_QOH", "ING_MIN"};

            @Override
            public String getColumnName(int column) {
                return columnnames[column];
            }

            @Override
            public int getRowCount() {
                return list.size();
            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Ingredient ingredient = list.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return ingredient.getCode();
                    case 1:
                        return ingredient.getIngName();
                    case 2:
                        return ingredient.getIngPrice();
                    case 3:
                        return ingredient.getIngType();
                    case 4:
                        return ingredient.getIngQoh();
                    case 5:
                        return ingredient.getIngMin();
                    default:
                        return "Unknown";
                }
            }
        });
        jScrollPane1.setViewportView(tblIngredient);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        labelEMPCODE = new javax.swing.JLabel();
        edtSearch = new javax.swing.JTextField();
        btnSearch = new javax.swing.JButton();
        btnRefresh = new javax.swing.JButton();
        btnCheckList = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblIngredient = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        btnDel = new javax.swing.JButton();
        btnAdd = new javax.swing.JButton();
        btnEdit1 = new javax.swing.JButton();

        jPanel1.setBackground(new java.awt.Color(204, 204, 255));

        labelEMPCODE.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        labelEMPCODE.setText("Ingredient_Code:");

        edtSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtSearchActionPerformed(evt);
            }
        });

        btnSearch.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        btnSearch.setText("Search");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        btnRefresh.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        btnRefresh.setText("Refresh");
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });

        btnCheckList.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        btnCheckList.setText("CheckList");
        btnCheckList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCheckListActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(labelEMPCODE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(edtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(262, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnCheckList, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelEMPCODE)
                    .addComponent(edtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(63, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSearch)
                    .addComponent(btnRefresh)
                    .addComponent(btnCheckList))
                .addContainerGap())
        );

        tblIngredient.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblIngredient);

        jPanel2.setBackground(new java.awt.Color(204, 204, 255));

        btnDel.setText("Delete");
        btnDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDelActionPerformed(evt);
            }
        });

        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnEdit1.setText("Edit");
        btnEdit1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEdit1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(113, 113, 113)
                .addComponent(btnEdit1, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnDel, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addGap(16, 16, 16)
                    .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(486, Short.MAX_VALUE)))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnDel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnEdit1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(btnAdd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 221, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void edtSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtSearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edtSearchActionPerformed

    private void btnDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDelActionPerformed
        int selectedIndex = tblIngredient.getSelectedRow();
        if (selectedIndex >= 0) {
            editedIngredient = list.get(selectedIndex);
            int input = JOptionPane.showConfirmDialog(this, "Do you want to proceed?", "Select an Option...",
                    JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
            if (input == 0) {
                ingredientService.delete(editedIngredient);
            }
            refreshTable();
        }
    }//GEN-LAST:event_btnDelActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        String codeText = edtSearch.getText();
        int code = Integer.parseInt(codeText);
        System.out.println(code);

        DefaultTableModel model = new DefaultTableModel(new Object[]{"ING_CODE","ING_NAME", "ING_PRICE", "ING_TYPE", "ING_QOH", "ING_MIN"}, 0);

        for (Ingredient ingredient : list) {
            if (ingredient.getCode() == code) {
                model.addRow(new Object[]{ingredient.getCode(),ingredient.getIngName(), ingredient.getIngPrice(), ingredient.getIngType(), ingredient.getIngQoh(), ingredient.getIngMin()});
                code = ingredient.getCode();
            }
        }
        isSearch = 1; SearchInd = code-1;
        tblIngredient.setModel(model);
    }//GEN-LAST:event_btnSearchActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        editedIngredient = new Ingredient();
        openDialog();
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnEdit1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEdit1ActionPerformed
        int selectedindex = tblIngredient.getSelectedRow();
        System.out.println(selectedindex);
        if(selectedindex >= 0 && isSearch == 0){
            editedIngredient = list.get(selectedindex);
            openDialog();
        }
        else if(isSearch == 1){
            Object ind = tblIngredient.getValueAt(0, 0);
            String str = ind.toString();
            int num = Integer.parseInt(str);
            System.out.println(num);
            
            Ingredient ingredient;
            IngredientDao ids = new  IngredientDao();
            int row= 0;
            for(Ingredient ig: ids.getAll()){
            System.out.println(ig.getCode());
            if(num == ig.getCode()){
                editedIngredient = list.get(row);
                openDialog();
                refreshTable();
            }
            row += 1;
        }
        }
    }//GEN-LAST:event_btnEdit1ActionPerformed

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        DefaultTableModel model = new DefaultTableModel(new Object[]{ "ING_CODE","ING_NAME", "ING_PRICE", "ING_TYPE", "ING_QOH", "ING_MIN"}, 0);

        for (Ingredient ingredient : list) {
            model.addRow(new Object[]{ingredient.getCode(),ingredient.getIngName(), ingredient.getIngPrice(), ingredient.getIngType(), ingredient.getIngQoh(), ingredient.getIngMin()});
        }
        edtSearch.setText(null);
        tblIngredient.setModel(model);
    }//GEN-LAST:event_btnRefreshActionPerformed

    private void btnCheckListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCheckListActionPerformed
        editedCheckStock = new Ingredient();
        openCheckDialog();
    }//GEN-LAST:event_btnCheckListActionPerformed

    private javax.swing.JScrollPane scrMainMenu;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnCheckList;
    private javax.swing.JButton btnDel;
    private javax.swing.JButton btnEdit1;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JButton btnSearch;
    private javax.swing.JTextField edtSearch;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelEMPCODE;
    private javax.swing.JTable tblIngredient;
    // End of variables declaration//GEN-END:variables

    private void openDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        IngredientDialog ingredientDialog = new IngredientDialog(frame, editedIngredient);
        ingredientDialog.setLocationRelativeTo(this);
        ingredientDialog.setVisible(true);
        ingredientDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTable();
            }
        });
    }
    
     private void openCheckDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        CheckStockDialog checkstockDialog = new CheckStockDialog(frame, editedCheckStock);
        checkstockDialog.setLocationRelativeTo(this);
        checkstockDialog.setVisible(true);
        checkstockDialog.addWindowListener(new WindowAdapter() {
        });
    }

    private void refreshTable() {
        isSearch = 0; SearchInd = 0;
        list = ingredientService.getIngredient();
        tblIngredient.revalidate();
        tblIngredient.repaint();
        System.out.println("Refresh Table");
    }
    
}
