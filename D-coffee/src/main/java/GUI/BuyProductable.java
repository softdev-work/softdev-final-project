/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package GUI;

import model.Product;

/**
 *
 * @author Sorawit Ketmart
 */
public interface BuyProductable {
    public void buy(Product product, int qty);
        
}
