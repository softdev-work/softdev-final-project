/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package GUI;

import Service.EmployeeService;
import com.mycompany.d.coffee.Dao.EmployeeDao;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import model.Employee;
import model.Ingredient;


public class EmployeePanel extends javax.swing.JPanel {
    
    
    private final EmployeeService employeeService;
    private List<Employee> list;
    private JTable tblEmployee;
    private Employee editedEmployee;
    private int isSearch;
    private int SearchPd;

    public EmployeePanel() {
        
        initComponents();   
        employeeService = new EmployeeService();
        list = employeeService.getEmployees();       
        tblEmployee = new JTable(new AbstractTableModel() {
            String[] columnnames = {"CODE","FNAME","LNAME","AGE","GENDER","PHONE","ADDRESS",
                "RANK","WAGESPERHOUR","EMAIL","TYPE","SALARY","INCOMETYPE","CODE",};      
            @Override
            public String getColumnName(int column) {
                return columnnames[column];
            }
            
            @Override
            public int getRowCount() {
                return list.size();
            }

            @Override
            public int getColumnCount() {
                return 14;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Employee employee = list.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return employee.getCode();
                    case 1:
                        return employee.getFname();
                    case 2:
                        return employee.getLname();
                    case 3:
                        return employee.getAge();
                    case 4:
                        return employee.getGender();
                    case 5:
                        return employee.getPhone();
                    case 6:
                        return employee.getAddress();
                    case 7:
                        return employee.getRank();
                    case 8:
                        return employee.getWagesPerHour();
                    case 9:
                        return employee.getEmail();
                    case 10:
                        return employee.getType();
                    case 11:
                        return employee.getSalary();
                    case 12:
                        return employee.getIncomeType();
                    case 13:
                        return employee.getBrcCode();                     
                    default:
                        return "Unknown";
                }
            }
        });
        jScrollPane1.setViewportView(tblEmployee);  
    }
    @SuppressWarnings("unchecked")
    
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        labelEMPCODE = new javax.swing.JLabel();
        edtEMPCODE = new javax.swing.JTextField();
        btnSearch = new javax.swing.JButton();
        btnRf = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tEMP = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        btnDel = new javax.swing.JButton();
        btnAdd = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();

        jPanel1.setBackground(new java.awt.Color(0, 102, 102));

        labelEMPCODE.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        labelEMPCODE.setText("Employee_Code:");

        edtEMPCODE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtEMPCODEActionPerformed(evt);
            }
        });

        btnSearch.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        btnSearch.setText("Search");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        btnRf.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        btnRf.setText("Refresh");
        btnRf.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRfActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(labelEMPCODE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(edtEMPCODE, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(261, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnRf, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelEMPCODE)
                    .addComponent(edtEMPCODE, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 63, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSearch)
                    .addComponent(btnRf))
                .addContainerGap())
        );

        tEMP.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tEMP);

        jPanel2.setBackground(new java.awt.Color(0, 102, 102));

        btnDel.setText("Delete");
        btnDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDelActionPerformed(evt);
            }
        });

        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnEdit.setText("Edit");
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(123, 123, 123)
                .addComponent(btnDel, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addGap(112, 112, 112)
                    .addComponent(btnEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(385, Short.MAX_VALUE)))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnDel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnAdd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(btnEdit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void edtEMPCODEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtEMPCODEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edtEMPCODEActionPerformed

    private void btnDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDelActionPerformed
        // TODO add your handling code here:
        int selectedIndex = tblEmployee.getSelectedRow();
        if (selectedIndex >= 0) {
            editedEmployee = list.get(selectedIndex);
            int input = JOptionPane.showConfirmDialog(this, "Do you want to Delete Employee?", "Select an Option...",
                    JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
            if (input == 0) {
                employeeService.delete(editedEmployee);
            }
        }
        refreshTable();
    }//GEN-LAST:event_btnDelActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        // TODO add your handling code here:
        editedEmployee = new Employee();
        openDialog();  
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        String codeText = edtEMPCODE.getText().toString();
        int code = Integer.parseInt(codeText);

        DefaultTableModel model = new DefaultTableModel(new Object[]{"CODE","FNAME","LNAME","AGE","GENDER","PHONE","ADDRESS",
                "RANK","WAGESPERHOUR","EMAIL","TYPE","SALARY","INCOMETYPE","CODE",}, 0);

        for (Employee employee : list) {
            if (employee.getCode() == code) {
                model.addRow(new Object[]{employee.getCode(), employee.getFname(), employee.getLname(), employee.getAge(), employee.getGender(), employee.getPhone(), employee.getAddress(), employee.getRank()
                        , employee.getWagesPerHour(), employee.getEmail(),employee.getType(), employee.getSalary(), employee.getIncomeType(), employee.getBrcCode()});
            }
        }
        isSearch = 1; SearchPd = code-1;
        tblEmployee.setModel(model);
        refreshTable();
        
    }//GEN-LAST:event_btnSearchActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        // TODO add your handling code here:
        int selectedindex = tblEmployee.getSelectedRow();
        System.out.println(selectedindex);
        if(selectedindex >= 0 && isSearch == 0){
            editedEmployee = list.get(selectedindex);
            openDialog();
        }
        else if(isSearch == 1){
            System.out.println("Hello "+SearchPd);
            Object ind = tblEmployee.getValueAt(0, 0);
            String str = ind.toString();
            int num = Integer.parseInt(str);
            System.out.println(num);
            
            Employee employee ;
            EmployeeDao ids = new  EmployeeDao();
            int row= 0;
            for(Employee ig: ids.getAll()){
            System.out.println(ig.getCode());
            if(num == ig.getCode()){
                editedEmployee = list.get(row);
            }
            row += 1;
        }
            openDialog();
        }
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnRfActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRfActionPerformed
        // TODO add your handling code here:
        DefaultTableModel model = new DefaultTableModel(new Object[]{"CODE","FNAME","LNAME","AGE","GENDER","PHONE","ADDRESS",
                "RANK","WAGESPERHOUR","EMAIL","TYPE","SALARY","INCOMETYPE","CODE",}, 0);

        for (Employee employee : list) {
            model.addRow(new Object[]{employee.getCode(), employee.getFname(), employee.getLname(), employee.getAge(), employee.getGender(), employee.getPhone(), employee.getAddress(), employee.getRank()
                        , employee.getWagesPerHour(), employee.getEmail(),employee.getType(), employee.getSalary(), employee.getIncomeType(), employee.getBrcCode()});
        }
        edtEMPCODE.setText(null);
        tblEmployee.setModel(model);
        refreshTable();
    }//GEN-LAST:event_btnRfActionPerformed
    
    private javax.swing.JScrollPane scrMainMenu;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnDel;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnRf;
    private javax.swing.JButton btnSearch;
    private javax.swing.JTextField edtEMPCODE;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelEMPCODE;
    private javax.swing.JTable tEMP;
    // End of variables declaration//GEN-END:variables
    private void refreshTable() {
        list = employeeService.getEmployees();
        tblEmployee.revalidate();
        tblEmployee.repaint();
        System.out.println("Refresh Table");
    }
    private void openDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        EmployeeDialog employeeDialog = new EmployeeDialog(frame, editedEmployee);
        employeeDialog.setLocationRelativeTo(this);
        employeeDialog.setVisible(true);
        employeeDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTable();
            }
        });
    }
}
