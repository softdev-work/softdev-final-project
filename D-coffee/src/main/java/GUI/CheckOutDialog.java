/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package GUI;

import Service.CheckWorkService;
import com.mycompany.d.coffee.Dao.EmployeeDao;
import java.awt.Window;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import model.CheckWork;
import model.Employee;

/**
 *
 * @author Asus
 */
public class CheckOutDialog extends javax.swing.JDialog {

    /**
     * Creates new form IngredientDialog
     */
    private final CheckWorkService cwService;
    private List<CheckWork> list;
    private CheckWork editedCw;

    public CheckOutDialog(java.awt.Frame parent, CheckWork editedCw) {
        super(parent, true);
        initComponents();
        this.editedCw = editedCw;
        setObjectToForm();
        cwService = new CheckWorkService();
        enableForm(false);
    }

    CheckOutDialog(JFrame frame, CheckWork editedCw) {
        initComponents();
        this.editedCw = editedCw;
        setObjectToForm();
        cwService = new CheckWorkService();
        enableForm(false);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btnLogin = new javax.swing.JButton();
        lblNo = new javax.swing.JLabel();
        edtEmail = new javax.swing.JTextField();
        lblempcode = new javax.swing.JLabel();
        lblTimeout = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        edtPassword = new javax.swing.JPasswordField();
        edtName = new javax.swing.JTextField();
        edtRank = new javax.swing.JTextField();
        edtcode = new javax.swing.JTextField();
        lblName = new javax.swing.JLabel();
        edtOut = new javax.swing.JTextField();
        btnSave = new javax.swing.JButton();
        lblInfo = new javax.swing.JLabel();
        lblStatus = new javax.swing.JLabel();
        lblDateout2 = new javax.swing.JLabel();
        lblEmpcode = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 153, 153));

        btnLogin.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        btnLogin.setText("Login");
        btnLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoginActionPerformed(evt);
            }
        });

        lblNo.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        lblNo.setText("Email :");

        edtEmail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtEmailActionPerformed(evt);
            }
        });

        lblempcode.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        lblempcode.setText("Password :");

        lblTimeout.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        lblTimeout.setText("Check  TimeOut");

        jLabel2.setText("Please fill data");

        edtName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtNameActionPerformed(evt);
            }
        });

        edtRank.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtRankActionPerformed(evt);
            }
        });

        edtcode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtcodeActionPerformed(evt);
            }
        });

        lblName.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        lblName.setText("Name :");

        edtOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtOutActionPerformed(evt);
            }
        });

        btnSave.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        lblInfo.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        lblInfo.setText("Infomation");

        lblStatus.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        lblStatus.setText("Status :");

        lblDateout2.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        lblDateout2.setText("Date  Out :");

        lblEmpcode.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        lblEmpcode.setText("Emp code :");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblempcode, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblNo, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(edtEmail, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                                    .addComponent(edtPassword))
                                .addGap(18, 18, 18))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(btnLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(66, 66, 66)))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblName, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblEmpcode, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblDateout2, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(121, 121, 121)
                        .addComponent(lblTimeout))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(167, 167, 167)
                        .addComponent(jLabel2)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(edtOut, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(edtRank, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(edtName, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(edtcode, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addComponent(lblInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(68, 68, 68)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(edtcode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblEmpcode))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(edtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblName))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(edtRank, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblStatus))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(edtOut, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblDateout2)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblTimeout)
                            .addComponent(lblInfo))
                        .addGap(12, 12, 12)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblNo)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lblempcode))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(edtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(edtPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnLogin)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 25, Short.MAX_VALUE)
                .addComponent(btnSave)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void edtEmailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtEmailActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edtEmailActionPerformed

    private void btnLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoginActionPerformed

        String inputEmail = edtEmail.getText();
        char[] passwordCharArray = edtPassword.getPassword();
        String inputPassword = new String(passwordCharArray);
        if (inputEmail.isEmpty() || inputPassword.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Please enter both email and password", "Empty", JOptionPane.WARNING_MESSAGE);
        } else {
            boolean isValidCredentials = isEmployeeCredentialsValid(inputEmail, inputPassword);
            if (isValidCredentials) {
                int employeeCode = getEmployeeCode(inputEmail, inputPassword);
                String employeeRank = getEmployeeRank(inputEmail, inputPassword);
                String Username = getLoggedInUsername(inputEmail, inputPassword);
                EnterToautoTime();
                edtcode.setText(Integer.toString(employeeCode));
                edtRank.setText(employeeRank);
                edtName.setText(Username);
            } else {
                JOptionPane.showMessageDialog(this, "Invalid email or password", "Login Failed", JOptionPane.WARNING_MESSAGE);
            }
        }
    }//GEN-LAST:event_btnLoginActionPerformed
    private void enableForm(boolean status) {
        edtcode.setEnabled(false);
        edtName.setEnabled(false);
        edtRank.setEnabled(false);
        edtOut.setEnabled(false);
    }
    private void edtNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edtNameActionPerformed

    private void edtRankActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtRankActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edtRankActionPerformed

    private void edtcodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtcodeActionPerformed
        edtcode.setEditable(false);
    }//GEN-LAST:event_edtcodeActionPerformed

    private void edtOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtOutActionPerformed
        EnterToautoTime();
    }//GEN-LAST:event_edtOutActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        try {
            setFormToObject();

            if (editedCw.getCode() < 0) { // Add New
                cwService.addNew(editedCw);
            } else {
                cwService.update(editedCw);
            }

            this.dispose();

        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, "Eror " + e.getMessage(), "EROR", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnSaveActionPerformed

    public boolean isEmployeeCredentialsValid(String email, String password) {
        boolean valid = false;
        try {
            Connection conn = DriverManager.getConnection("jdbc:sqlite:Mockup.db");
            String sql = "SELECT * FROM EMPLOYEE WHERE EMP_EMAIL = ? AND EMP_PASSWORD = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                valid = true;
            }
            rs.close();
            preparedStatement.close();
            conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return valid;
    }

    public static Employee fromRS(ResultSet rs) {
        Employee employee = new Employee();
        try {
            employee.setCode(rs.getInt("EMP_CODE"));
            employee.setFname(rs.getString("EMP_FNAME"));
            employee.setLname(rs.getString("EMP_LNAME"));
            employee.setAge(rs.getInt("EMP_AGE"));
            employee.setGender(rs.getString("EMP_GENDER"));
            employee.setPhone(rs.getString("EMP_PHONE"));
            employee.setAddress(rs.getString("EMP_ADDRESS"));
            employee.setRank(rs.getString("EMP_RANK"));
            employee.setWagesPerHour(rs.getFloat("EMP_WAGES_PER_HOUR"));
            employee.setEmail(rs.getString("EMP_EMAIL"));
            employee.setType(rs.getString("EMP_TYPE"));
            employee.setSalary(rs.getFloat("EMP_SALARY"));
            employee.setIncomeType(rs.getString("EMP_INCOME_TYPE"));
            employee.setPassword(rs.getString("EMP_Password"));
            employee.setBrcCode(rs.getInt("BRC_CODE"));
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return employee;
    }

    public static CheckWork fromRSCheckWork(ResultSet rs) throws ParseException {
        CheckWork checkWork = new CheckWork();
        try {
            checkWork.setCode(rs.getInt("CW_CODE"));
            checkWork.setDate(rs.getTimestamp("CW_DATE"));
            checkWork.setTimein(rs.getString("CW_TIMEIN"));
            checkWork.setTimeout(rs.getString("CW_TIMEOUT"));
            checkWork.setEmployee(rs.getInt("EMP_CODE"));

            EmployeeDao employeeDao = new EmployeeDao();
            Employee employee = employeeDao.get(checkWork.getCode());
        } catch (SQLException ex) {
            Logger.getLogger(CheckWork.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkWork;
    }

    public boolean isCheck(String date, int code) {
        boolean valid = false;
        try {
            Connection conn = DriverManager.getConnection("jdbc:sqlite:Mockup.db");
            String sql = "UPDATE CHECKWORK SET CW_TIMEOUT = ? WHERE EMP_CODE = ? AND CW_DATE = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, date);
            preparedStatement.setInt(2, code);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                valid = true;
            }
            rs.close();
            preparedStatement.close();
            conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return valid;
    }

    private String getEmployeeRank(String email, String password) {
        String rank = null;
        try {
            Connection conn = DriverManager.getConnection("jdbc:sqlite:Mockup.db");
            String sql = "SELECT EMP_RANK FROM EMPLOYEE WHERE EMP_EMAIL = ? AND EMP_PASSWORD = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                rank = rs.getString("EMP_RANK");
            }
            rs.close();
            preparedStatement.close();
            conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return rank;
    }

    public String getLoggedInUsername(String email, String password) {
        String username = null;
        try {
            Connection conn = DriverManager.getConnection("jdbc:sqlite:Mockup.db");
            String sql = "SELECT EMP_FNAME, EMP_LNAME FROM EMPLOYEE WHERE EMP_EMAIL = ? AND EMP_PASSWORD = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                String firstName = rs.getString("EMP_FNAME");
                String lastName = rs.getString("EMP_LNAME");
                username = firstName + " " + lastName;
            }
            rs.close();
            preparedStatement.close();
            conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return username;
    }

    private int getEmployeeCode(String email, String password) {
        int employeeCode = -1;

        try {
            Connection conn = DriverManager.getConnection("jdbc:sqlite:Mockup.db");
            String sql = "SELECT EMP_CODE FROM EMPLOYEE WHERE EMP_EMAIL = ? AND EMP_PASSWORD = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                employeeCode = rs.getInt("EMP_CODE");
            }
            rs.close();
            preparedStatement.close();
            conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return employeeCode;
    }

    private void EnterToautoTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        Date currentTime = new Date();
        String formattedTime = dateFormat.format(currentTime);
        edtOut.setText(formattedTime);
    }

    private void EnterToautoTimeOut() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        Date currentTime = new Date();
        String formattedTime = dateFormat.format(currentTime);
        //edtOut.setText(formattedTime);
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnLogin;
    private javax.swing.JButton btnSave;
    private javax.swing.JTextField edtEmail;
    private javax.swing.JTextField edtName;
    private javax.swing.JTextField edtOut;
    private javax.swing.JPasswordField edtPassword;
    private javax.swing.JTextField edtRank;
    private javax.swing.JTextField edtcode;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblDateout2;
    private javax.swing.JLabel lblEmpcode;
    private javax.swing.JLabel lblInfo;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblNo;
    private javax.swing.JLabel lblStatus;
    private javax.swing.JLabel lblTimeout;
    private javax.swing.JLabel lblempcode;
    // End of variables declaration//GEN-END:variables

    private void setFormToObject() {
        editedCw.setTimeout(edtOut.getText());
    }

    private void setObjectToForm() {

    }

}
