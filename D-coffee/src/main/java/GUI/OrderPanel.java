/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package GUI;

import Service.OrderingDetailsService;
import Service.OrderingService;
import com.mycompany.d.coffee.Dao.OrderingDao;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import model.Ordering;
import model.OrderingDetails;

public class OrderPanel extends javax.swing.JPanel {

    private OrderingService OrderingService;
    private OrderingDetailsService OrderingDetailsService;
    private List<Ordering> list;
    private List<OrderingDetails> lists;
    private Ordering editedOrd;
    private OrderingDetails editedView;
    private int showDetail;
    private boolean view;
    private int isSearch;
    private int SearchOrd;

    public OrderPanel() {
        initComponents();
        OrderingService = new OrderingService();
        OrderingDetailsService = new OrderingDetailsService();
        list = OrderingService.getOrdering();
        lists = OrderingDetailsService.getOrderingDetails();
        tblOrder = new JTable(new AbstractTableModel() {
            String[] columnnames = {"CODE", "DATE", "AMOUNT", "TOTAL PRICE", "VENDOR", "DISCOUNT", "EMP_CODE"};

            @Override
            public String getColumnName(int column) {
                return columnnames[column];
            }

            @Override
            public int getRowCount() {
                return list.size();
            }

            @Override
            public int getColumnCount() {
                return 7;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Ordering ordering = list.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return ordering.getOrdCode();
                    case 1:
                        return ordering.getOrdDate();
                    case 2:
                        return ordering.getOrdAmount();
                    case 3:
                        return ordering.getOrdPrice();
                    case 4:
                        return ordering.getOrdVendor();
                    case 5:
                        return ordering.getOrdDiscount();
                    case 6:
                        return ordering.getEmployee();
                    default:
                        return "Unknown";
                }
            }
        });
        jScrollPane1.setViewportView(tblOrder);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        labelEMPCODE = new javax.swing.JLabel();
        edtBr = new javax.swing.JTextField();
        btnRefresh = new javax.swing.JButton();
        btnSearch = new javax.swing.JButton();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 32767));
        btnView = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblOrder = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        btnEdit = new javax.swing.JButton();
        btnAdd = new javax.swing.JButton();
        btnDel1 = new javax.swing.JButton();

        jPanel1.setBackground(new java.awt.Color(204, 204, 255));

        labelEMPCODE.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        labelEMPCODE.setText("ORDER CODE:");

        edtBr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtBrActionPerformed(evt);
            }
        });

        btnRefresh.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        btnRefresh.setText("Refresh");
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });

        btnSearch.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        btnSearch.setText("Search");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        btnView.setText("View");
        btnView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(labelEMPCODE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(edtBr, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(270, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(168, 168, 168)
                .addComponent(filler1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnView, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelEMPCODE)
                    .addComponent(edtBr, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 49, Short.MAX_VALUE)
                .addComponent(filler1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSearch)
                    .addComponent(btnRefresh)
                    .addComponent(btnView, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        tblOrder.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblOrder);

        jPanel2.setBackground(new java.awt.Color(204, 204, 255));

        btnEdit.setText("Edit");
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnDel1.setText("Delete");
        btnDel1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDel1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addGap(208, 208, 208)
                    .addComponent(btnDel1, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(289, Short.MAX_VALUE)))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnEdit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnAdd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(btnDel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 587, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 702, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void edtBrActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtBrActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edtBrActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        int selectedindex = tblOrder.getSelectedRow();
        System.out.println(selectedindex);
        if(selectedindex >= 0 && isSearch == 0){
            editedOrd = list.get(selectedindex);
            openDialog();
            System.out.println("scenario1");
        }
        else if(isSearch == 1){
            System.out.println("Hello "+SearchOrd);
            Object ind = tblOrder.getValueAt(0, 0);
            String str = ind.toString();
            int num = Integer.parseInt(str);
            System.out.println(num);
            
            Ordering product;
            OrderingDao ids = new  OrderingDao();
            int row= 0;
            for(Ordering ig: ids.getAll()){
            System.out.println(ig.getOrdCode());
            if(num == ig.getOrdCode()){
                editedOrd = list.get(row);
            }
            row += 1;
        }
            openDialog();
        }
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        if (view) {
            editedView = new OrderingDetails();
            openViewDialog();
            
        } else {
            editedOrd = new Ordering();
            System.out.println("open add");
            openDialog();
        }
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        DefaultTableModel model = new DefaultTableModel(new Object[]{"CODE", "DATE", "AMOUNT", "TOTAL PRICE", "VENDOR", "DISCOUNT", "EMP_CODE"}, 0);

        for (Ordering B : list) {
            model.addRow(new Object[]{B.getOrdCode(), B.getOrdDate(), B.getOrdAmount(), B.getOrdPrice(), B.getOrdVendor(), B.getOrdDiscount(), B.getEmployee()});
        }
        view = false;
        edtBr.setText(null);
        tblOrder.setModel(model);
        refreshTable();
    }//GEN-LAST:event_btnRefreshActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        String codeText = edtBr.getText();
        int code = Integer.parseInt(codeText);

        DefaultTableModel model = new DefaultTableModel(new Object[]{"CODE", "DATE", "AMOUNT", "TOTAL PRICE", "VENDOR", "DISCOUNT", "EMP_CODE"}, 0);

        for (Ordering B : list) {
            if (B.getOrdCode() == code) {
                model.addRow(new Object[]{B.getOrdCode(), B.getOrdDate(), B.getOrdAmount(), B.getOrdPrice(), B.getOrdVendor(), B.getOrdDiscount(), B.getEmployee()});
            }
        }
        tblOrder.setModel(model);
    }//GEN-LAST:event_btnSearchActionPerformed

    private void btnViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewActionPerformed

        view = true;
        int getnum = tblOrder.getSelectedRow();
        Object ind = tblOrder.getValueAt(getnum, 0);
        String str = ind.toString();
        int num = Integer.parseInt(str);

        DefaultTableModel model = new DefaultTableModel(new Object[]{"CODE", "NAME", "AMOUNT", "UNIT PRICE", "DISCOUNT", "TOTAL NETPRICE", "ORD_D CODE"}, 0);

        for (OrderingDetails B : lists) {
            if (B.getOrdCode() == num) {
                model.addRow(new Object[]{B.getOrdCode(), B.getName(), B.getAmount(), B.getUnitPrice(), B.getDiscount(), B.getNetPrice(), B.getCode()});
            }
        }
        tblOrder.setModel(model);
    }//GEN-LAST:event_btnViewActionPerformed

    private void btnDel1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDel1ActionPerformed
        int selectedIndex = tblOrder.getSelectedRow();

        if (selectedIndex >= 0) {
            editedOrd = list.get(selectedIndex);
            int input = JOptionPane.showConfirmDialog(this, "Do you want to proceed?", "Select an Option...",
                    JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
            if (input == 0) {
                System.out.println("input == 0");
                if (view) {
                    int getnum = tblOrder.getSelectedRow();
                    Object ind = tblOrder.getValueAt(getnum, 6);
                    String str = ind.toString();
                    int num = Integer.parseInt(str);

                    int row = 0;
                    for (OrderingDetails B : lists) {
                        if (B.getCode() == num) {
                            editedView = lists.get(row);
                        }
                        row++;
                    }

                    System.out.println("delete view");
                    OrderingDetailsService.delete(editedView);
                    refreshView();
                } else {
                    int getnum = tblOrder.getSelectedRow();
                    Object ind = tblOrder.getValueAt(getnum, 0);
                    String str = ind.toString();
                    int num = Integer.parseInt(str);

                    int row = 0;
                    for (OrderingDetails B : lists) {
                        if (B.getCode() == num) {
                            editedView = lists.get(row);
                        }
                        row++;
                    }

                    OrderingService.delete(editedOrd);
                    refreshTable();
                }
            }

        }
    }//GEN-LAST:event_btnDel1ActionPerformed

    private javax.swing.JScrollPane scrMainMenu;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnDel1;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JButton btnSearch;
    private javax.swing.JButton btnView;
    private javax.swing.JTextField edtBr;
    private javax.swing.Box.Filler filler1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelEMPCODE;
    private javax.swing.JTable tblOrder;
    // End of variables declaration//GEN-END:variables

    private void openDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        OrderingDialog ordDialog = new OrderingDialog(frame, editedOrd);
        ordDialog.setLocationRelativeTo(this);
        ordDialog.setVisible(true);
        ordDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTable();
                refreshView();
                refreshTable();
            }
        });
    }

    private void openViewDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        OrderingViewDialog ordDialog = new OrderingViewDialog(frame, editedView);
        ordDialog.setLocationRelativeTo(this);
        ordDialog.setVisible(true);
        ordDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshView();
                System.out.println("Reset View");
            }
        });
    }

    private void refreshTable() {
        list = OrderingService.getOrdering();
        tblOrder.revalidate();
        tblOrder.repaint();
        DefaultTableModel model = new DefaultTableModel(new Object[]{"CODE", "DATE", "AMOUNT", "TOTAL PRICE", "VENDOR", "DISCOUNT", "EMP_CODE"}, 0);

        for (Ordering B : list) {
            model.addRow(new Object[]{B.getOrdCode(), B.getOrdDate(), B.getOrdAmount(), B.getOrdPrice(), B.getOrdVendor(), B.getOrdDiscount(), B.getEmployee()});
        }
        edtBr.setText(null);
        tblOrder.setModel(model);
        System.out.println("Refresh Table");
    }

    private void refreshView() {
        lists = OrderingDetailsService.getOrderingDetails();
        tblOrder.revalidate();
        tblOrder.repaint();
        System.out.println("Refresh ord Details Table");
        Object ind = tblOrder.getValueAt(0, 0);
        String str = ind.toString();
        int num = Integer.parseInt(str);

        DefaultTableModel model = new DefaultTableModel(new Object[]{"CODE", "NAME", "AMOUNT", "UNIT PRICE", "DISCOUNT", "TOTAL NETPRICE","ORD_D CODE"}, 0);

        for (OrderingDetails B : lists) {
            if (B.getOrdCode() == num) {
                model.addRow(new Object[]{B.getOrdCode(), B.getName(), B.getAmount(), B.getUnitPrice(), B.getDiscount(), B.getNetPrice(), B.getCode()});
            }
        }
        tblOrder.setModel(model);
    }

}
