/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package GUI;

import Service.ReportProfitService;
import Service.ReportService;
import java.awt.Dimension;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Properties;
import javax.swing.table.AbstractTableModel;
import model.DateLabelFormatter;
import model.Report;
import model.ReportProfit;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

/**
 *
 * @author sTCFILMs
 */
public class ReportPanel extends javax.swing.JPanel {

    private final ReportService productService;
    private List<Report> productlist;
    private AbstractTableModel model;
    private UtilDateModel model1;
    private UtilDateModel model2;
    private DefaultCategoryDataset Bardataset;
    private ReportProfitService reportprofitService;
    private List<ReportProfit> reportprofitList;
    private AbstractTableModel model3;
    private DefaultPieDataset Piedataset;

    /**
     * Creates new form Report1
     */
    public ReportPanel() {
        initComponents();
        productService = new ReportService();
        productlist = productService.getProductByAmount();
        inittable();
        initdatepicker();
        initBarChart();
        loadBardataset();
        reportprofitService = new ReportProfitService();
        reportprofitList = reportprofitService.getprofit_loss();
        model3 = new AbstractTableModel() {

            String[] colnames = {"INCOME", "EXPENSE","Result"};

            @Override
            public String getColumnName(int column) {
                return colnames[column];
            }

            @Override
            public int getRowCount() {
                return reportprofitList.size();
            }

            @Override
            public int getColumnCount() {
                return 3;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ReportProfit profit = reportprofitList.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return profit.getIncome();
                    case 1:
                        return profit.getExpenses();
                    case 2:
                        return profit.getResult();
                    default:
                        return "";
                }
            }
        };
        tblprofit.setModel(model3);
        initPiedataset();
        loadPiedataset();
        
    }

    private void initPiedataset() {
        Piedataset = new DefaultPieDataset();
        JFreeChart chart = ChartFactory.createPieChart("Profit-Loss", // chart title
                Piedataset, // data
                true, // include legend
                true,
                false);
        ChartPanel chartPanel1 = new ChartPanel(chart);
        chartPanel1.setPreferredSize(new Dimension(583, 306));
        pnlPieGraph.add(chartPanel1);
    }

    private void loadPiedataset() {
        Piedataset.clear();
        for (ReportProfit p : reportprofitList) {
            Piedataset.setValue("Income", p.getIncome());
            Piedataset.setValue("expenses", p.getExpenses());
        }
    }

    private void initBarChart() {
        Bardataset = new DefaultCategoryDataset();
        JFreeChart chart = ChartFactory.createBarChart(
                "Best Seller Product",
                "Product",
                "Total",
                Bardataset,
                PlotOrientation.VERTICAL,
                true, true, false);
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new Dimension(583, 306));
        pnlBarGraph.add(chartPanel);
    }

    private void initdatepicker() {
        model1 = new UtilDateModel();
        Properties p1 = new Properties();
        p1.put("text.today", "Today");
        p1.put("text.month", "Month");
        p1.put("text.year", "year");
        JDatePanelImpl datePanel1 = new JDatePanelImpl(model1, p1);
        JDatePickerImpl datePicker1 = new JDatePickerImpl(datePanel1, new DateLabelFormatter());
        PanaelDatepicker1.add(datePicker1);
        model1.setSelected(true);
        model2 = new UtilDateModel();
        Properties p2 = new Properties();
        p2.put("text.today", "Today");
        p2.put("text.month", "Month");
        p2.put("text.year", "year");
        JDatePanelImpl datePanel2 = new JDatePanelImpl(model2, p2);
        JDatePickerImpl datePicker2 = new JDatePickerImpl(datePanel2, new DateLabelFormatter());
        PanaelDatepicker2.add(datePicker2);
        model2.setSelected(true);
    }

    private void inittable() {
        model = new AbstractTableModel() {
            String[] colname = {"CODE", "NAME", "TOTAL"};

            @Override
            public String getColumnName(int column) {
                return colname[column];
            }

            @Override
            public int getRowCount() {
                return productlist.size();
            }

            @Override
            public int getColumnCount() {
                return 3;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Report product = productlist.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return product.getCode();
                    case 1:
                        return product.getName();
                    case 2:
                        return product.getTotal();
                    default:
                        return "";
                }

            }
        };
        tblproduct.setModel(model);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        PanaelDatepicker1 = new javax.swing.JPanel();
        PanaelDatepicker2 = new javax.swing.JPanel();
        btnprocess = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblproduct = new javax.swing.JTable();
        pnlBarGraph = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblprofit = new javax.swing.JTable();
        pnlPieGraph = new javax.swing.JPanel();
        lblTopten = new javax.swing.JLabel();
        lblProfit = new javax.swing.JLabel();

        jPanel1.setBackground(new java.awt.Color(0, 255, 204));

        btnprocess.setText("Process");
        btnprocess.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnprocessActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(PanaelDatepicker1, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(PanaelDatepicker2, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnprocess, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnprocess, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(PanaelDatepicker2, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
                    .addComponent(PanaelDatepicker1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(20, Short.MAX_VALUE))
        );

        tblproduct.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblproduct);

        tblprofit.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblprofit);

        lblTopten.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        lblTopten.setText("Top Ten Best Seller Product");

        lblProfit.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        lblProfit.setText("Profit-Loss");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 332, Short.MAX_VALUE)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addComponent(lblTopten))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(94, 94, 94)
                        .addComponent(lblProfit)))
                .addGap(65, 65, 65)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(pnlBarGraph, javax.swing.GroupLayout.DEFAULT_SIZE, 583, Short.MAX_VALUE)
                    .addComponent(pnlPieGraph, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(33, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(pnlBarGraph, javax.swing.GroupLayout.PREFERRED_SIZE, 306, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(7, 7, 7)
                        .addComponent(lblTopten)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblProfit)))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlPieGraph, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 231, Short.MAX_VALUE))))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnprocessActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnprocessActionPerformed
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat formater = new SimpleDateFormat(pattern);
        System.out.println("" + formater.format(model1.getValue()) + " " + formater.format(model2.getValue()));
        String begin = formater.format(model1.getValue());
        String end = formater.format(model2.getValue());
        productlist = productService.getProductByAmount(begin, end);
        model.fireTableDataChanged();
        loadBardataset();
        reportprofitList = reportprofitService.getProfit_loss(begin, end);
        model3.fireTableDataChanged();
        loadPiedataset();
    }//GEN-LAST:event_btnprocessActionPerformed
    private void loadBardataset() {
        Bardataset.clear();
        for (Report p : productlist) {
            Bardataset.setValue(p.getTotal(), "Total", p.getName());
        }
    }
//    private void updateTableData() {
//    model.fireTableDataChanged();
//}
//    private void updateBarChart() {
//    Bardataset.clear();
//    for (Report p : productlist) {
//        Bardataset.setValue(p.getTotal(), "Total", p.getName());
//    }
//}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel PanaelDatepicker1;
    private javax.swing.JPanel PanaelDatepicker2;
    private javax.swing.JButton btnprocess;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblProfit;
    private javax.swing.JLabel lblTopten;
    private javax.swing.JPanel pnlBarGraph;
    private javax.swing.JPanel pnlPieGraph;
    private javax.swing.JTable tblproduct;
    private javax.swing.JTable tblprofit;
    // End of variables declaration//GEN-END:variables
}
