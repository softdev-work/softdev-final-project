/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Test;

import com.mycompany.d.coffee.Dao.ProductDao;
import com.mycompany.d.coffee.Dao.PromotionDao;
import com.mycompany.d.coffee.Helper.DatabaseHelper;
import model.Promotion;

/**
 *
 * @author informatics
 */
public class TestPromotionDao {
    public static void main(String[] args) {
        PromotionDao pdDao = new PromotionDao();
        for(Promotion p : pdDao.getAll()){
            System.out.println(p);
        }
    }
}
