/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Test;

import Service.CustomerService;
import model.Customer;

/**
 *
 * @author Peach
 */
public class TestCustomerService {

    public static void main(String[] args) {
        CustomerService cm = new CustomerService();
        for (Customer customer : cm.getCustomer()) {
            System.out.println(customer);
        }    
        Customer cm1 = new Customer(31,"Peach", "Bunphanuk",20,"0934781717","Thailand","65160159@go.buu.ac.th",500);
        cm.addNew(cm1); 
        for (Customer customer : cm.getCustomer()) {
            System.out.println(customer);
        }
        
        Customer cm2 = new Customer(32,"TAO", "Suone",20,"0877889910","Thailand","65160183@go.buu.ac.th",600);
        cm.addNew(cm2); 
        for (Customer customer : cm.getCustomer()) {
            System.out.println(customer);
        }
        
        Customer cm3 = new Customer("BOOM", "GAK",20,"0233449910","Thailand","65160198@go.buu.ac.th",300);
        cm.addNew(cm3);
        for (Customer customer : cm.getCustomer()) {
            System.out.println(customer);
        }
       
        Customer cm4 = new Customer();
        cm.addNew(cm4); 
        for (Customer customer : cm.getCustomer()) {
            System.out.println(customer);
        }
        
        Customer upcm = cm.getByCode(32);
        upcm.setFname("Wave");
        cm.update(upcm);
        System.out.println("After Update");
        for (Customer customer : cm.getCustomer()) {
           System.out.println(customer);
        }
        
        Customer delcm = cm.getByCode(31);
        cm.update(delcm);
        cm.delete(delcm);
        System.out.println("After Update");
        for (Customer customer : cm.getCustomer()) {
            System.out.println(customer);
        }
    }
}
