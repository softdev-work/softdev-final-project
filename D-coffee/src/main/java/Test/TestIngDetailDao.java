/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Test;

import com.mycompany.d.coffee.Dao.CheckIngredientDao;
import com.mycompany.d.coffee.Dao.IngredientDao;
import com.mycompany.d.coffee.Dao.IngredientDetailDao;
import java.util.List;
import model.CheckIngredient;
import model.Ingredient;
import model.IngredientDetail;

/**
 *
 * @author Asus
 */
public class TestIngDetailDao {

    public static void main(String[] args) {
        IngredientDetailDao ingd = new IngredientDetailDao();

        //addNew 100% Success
        IngredientDao ing = new IngredientDao();
        CheckIngredientDao cid = new CheckIngredientDao();
        List<CheckIngredient> checks = cid.getAll();
        CheckIngredient checking0 = checks.get(0);
        Ingredient ingredient = ing.get(7);
//        IngredientDetail newIngredientDetail = new IngredientDetail(-1, ingredient.getIngName() ,ingredient.getIngQoh(), 50, 10, ingredient.getIngPrice(),0,10,checking0.getCode());
//        IngredientDetail newIngredientDetail = new IngredientDetail();
        IngredientDetail newIngredientDetail = new IngredientDetail(7, ingredient.getIngName() ,ingredient.getIngQoh(), 20, 8, ingredient.getIngPrice(),0,ingredient.getCode(),checking0.getCode());
        ingd.save(newIngredientDetail);
        
//        update Success
//        IngredientDetail ingre = ingd.getByCode(10);
//        ingre.setName("Honey Lemon");
//        ingre.setExp(2);
//        ingd.update(ingre);

//        delete Success
//        IngredientDetail ingre = ingd.getByCode(5);
//        ingd.delete(ingre);
            
         
        for (IngredientDetail igd : ingd.getAll()) {
            System.out.println(igd);
        }
    }
}
