/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Test;

import Service.PromotionService;
import model.Promotion;

/**
 *
 * @author iHC
 */
public class TestPromotionService {
    public static void main(String[] args) {
        PromotionService pm = new PromotionService();
        for (Promotion promotion : pm.getPromotions()) {
            System.out.println(promotion);
        }  
    }
}
