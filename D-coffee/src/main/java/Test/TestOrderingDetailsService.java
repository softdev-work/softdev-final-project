/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Test;

import Service.OrderingDetailsService;
import model.OrderingDetails;

/**
 *
 * @author informatics
 */
public class TestOrderingDetailsService {
    public static void main(String[] args) {
        OrderingDetailsService pm = new OrderingDetailsService();
        for (OrderingDetails orderingDetails : pm.getOrderingDetails()) {
            System.out.println(orderingDetails);
        }  
    }
}
