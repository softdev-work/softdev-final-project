/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Test;

import Service.PromotionDetailsService;
import model.PromotionDetails;

/**
 *
 * @author informatics
 */
public class TestPromotionDetailsService {
    public static void main(String[] args) {
        PromotionDetailsService pm = new PromotionDetailsService();
        for (PromotionDetails promotionDetails : pm.getPromotions()) {
            System.out.println(promotionDetails);
        }  
    }
}
