/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Test;

import com.mycompany.d.coffee.Dao.ProductDao;
import com.mycompany.d.coffee.Helper.DatabaseHelper;
import model.Product;

/**
 *
 * @author sTCFILMs
 */
public class TestProductDao {
    public static void main(String[] args) {
        ProductDao pdDao = new ProductDao();
//        for(Product p : pdDao.getAll()){
//            System.out.println(p);
//        }

        Product pd1 = pdDao.get(5);
        System.out.println(pd1);
        
// name product start with p DESC price
        for(Product p : pdDao.getAll(" PD_NAME like 'p%'"," PD_PRICE DESC ")) {
            System.out.println(p);
        }
        DatabaseHelper.close();
    }
}
