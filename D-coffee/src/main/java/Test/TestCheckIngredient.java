/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Test;

import Service.CheckIngredientService;
import java.text.ParseException;
import model.CheckIngredient;

/**
 *
 * @author Asus
 */
public class TestCheckIngredient {

    public static void main(String[] args) throws ParseException {
        CheckIngredientService cis = new CheckIngredientService();

        //addNew 
        CheckIngredient ci3 = new CheckIngredient(5,2);
//        CheckIngredient ci3 = new CheckIngredient();
//        CheckIngredient ci3 = new CheckIngredient(8);
        cis.addNew(ci3);
//        System.out.println(ci3);
        for (CheckIngredient cs : cis.getCheckIngredients()) {
            System.out.println(cs);
        }

    }
}
