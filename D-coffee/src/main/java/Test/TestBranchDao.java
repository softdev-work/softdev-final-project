/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Test;

import com.mycompany.d.coffee.Dao.BranchDao;
import com.mycompany.d.coffee.Helper.DatabaseHelper;
import model.Branch;

/**
 *
 * @author peakk
 */
public class TestBranchDao {
    public static void main(String[] args) {
        BranchDao branchdao = new BranchDao();
        System.out.println(branchdao.getAll());
        Branch branch1 = branchdao.get(2);
        System.out.println(branch1);
         
        
        //Branch newbranch = new Branch("G-Coffee","Ohio",10,"0113344788");
        //Branch insertedBranch = branchdao.save(newbranch); 
        //System.out.println(insertedBranch);
        branch1.setAdress("SSD");
        branchdao.update(branch1);
        Branch updateBranch = branchdao.get(branch1.getCode());
        System.out.println(updateBranch);
        
        branchdao.delete(branch1);
        for(Branch u: branchdao.getAll()){
            System.out.println(u);
        }
        DatabaseHelper.close();
    }
}
