/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Test;

import com.mycompany.d.coffee.Dao.OrderingDao;
import com.mycompany.d.coffee.Helper.DatabaseHelper;
import model.Ordering;

/**
 *
 * @author informatics
 */
public class TestOrderingDao {
    public static void main(String[] args) {
        OrderingDao ordDao = new OrderingDao();
        for(Ordering p : ordDao.getAll()){
            System.out.println(p);
        }
    }
}
