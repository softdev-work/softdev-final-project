/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Test;

import Service.IngredientService;
import model.Ingredient;

/**
 *
 * @author Asus
 */
public class TestIngredientService {
    public static void main(String[] args) {
        IngredientService is = new IngredientService();
        
//addNew Success
            Ingredient ing1 = new Ingredient(13,"Honey", (float) 12.50,"Type_2",150,20);
            is.addNew(ing1);

//           Ingredient ing1 = new Ingredient();
//           is.addNew(ing1);


//update Success
//        Ingredient udi = is.getByCode(1);
//        udi.setIngPrice((float) 100.00);
//        is.update(udi);
        
//delete Suc
//        Ingredient del = is.getByCode(12);
//        is.delete(del);

//        System.out.println(is.getByCode(18));
        for(Ingredient ing: is.getIngredient()){
            System.out.println(ing);
        }
    }
}
