/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Service;

import com.mycompany.d.coffee.Dao.RecieptDetailsDao;
import java.util.List;
import model.RecieptDetails;

/**
 *
 * @author ASUS
 */
public class RecieptDetailsService {

    public RecieptDetails getByCode(int id){
        RecieptDetailsDao recieptDao = new RecieptDetailsDao();
        return recieptDao.get(id);
    }
              
    public List<RecieptDetails> getRecieptDetailss(){
        RecieptDetailsDao recieptDao = new RecieptDetailsDao();
        return recieptDao.getAll(" RC_D_CODE asc");
    }

    public RecieptDetails addNew(RecieptDetails editedRecieptDetails) {
        RecieptDetailsDao recieptDao = new RecieptDetailsDao();
        return recieptDao.save(editedRecieptDetails);
    }

    public RecieptDetails update(RecieptDetails editedRecieptDetails) {
        RecieptDetailsDao recieptDao = new RecieptDetailsDao();
        return recieptDao.update(editedRecieptDetails);
    }

    public int delete(RecieptDetails editedRecieptDetails) {
        RecieptDetailsDao recieptDao = new RecieptDetailsDao();
        return recieptDao.delete(editedRecieptDetails);
    }
}
