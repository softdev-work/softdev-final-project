/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Service;

import com.mycompany.d.coffee.Dao.OrderingDao;
import java.util.List;
import model.Ordering;

/**
 *
 * @author iHC
 */
public class OrderingService {
    public Ordering getById(int id){
        OrderingDao orderingDao = new OrderingDao();
        return orderingDao.get(id);
    }
              
    public List<Ordering> getOrdering(){
        OrderingDao orderingDao = new OrderingDao();
        return orderingDao.getAll(" ORD_CODE asc");
    }

    public Ordering addNew(Ordering editedOrdering) {
        OrderingDao orderingDao = new OrderingDao();
        return orderingDao.save(editedOrdering);
    }

    public Ordering update(Ordering editedOrdering) {
        OrderingDao orderingDao = new OrderingDao();
        return orderingDao.update(editedOrdering);
    }

    public int delete(Ordering editedOrdering) {
        OrderingDao orderingDao = new OrderingDao();
        return orderingDao.delete(editedOrdering);
    }
}
