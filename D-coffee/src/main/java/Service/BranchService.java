/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Service;

import com.mycompany.d.coffee.Dao.BranchDao;
import java.util.List;
import model.Branch;

/**
 *
 * @author peakk
 */
public class BranchService {
    public Branch getByCode(int code){
        BranchDao branchdao = new BranchDao();
        return branchdao.get(code);
    }
              
    public List<Branch> getBranch(){
        BranchDao BranchDao = new BranchDao();
        return BranchDao.getAll(" BRC_CODE asc");
    }

    public Branch addNew(Branch editedBranch) {
        BranchDao BranchDao = new BranchDao();
        return BranchDao.save(editedBranch);
    }

    public Branch update(Branch editedBranch) {
        BranchDao BranchDao = new BranchDao();
        return BranchDao.update(editedBranch);
    }

    public int delete(Branch editedBranch) {
        BranchDao BranchDao = new BranchDao();
        return BranchDao.delete(editedBranch);
    }
}

    

