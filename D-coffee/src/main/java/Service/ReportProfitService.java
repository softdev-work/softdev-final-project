/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Service;

import com.mycompany.d.coffee.Dao.  ReportProfitDao;
import java.util.List;

import model. ReportProfit;

/**
 *
 * @author peakk
 */
public class  ReportProfitService {
    public List< ReportProfit> getprofit_loss() {
         ReportProfitDao reportdao= new  ReportProfitDao();
        return reportdao.getProfit_loss();
    }
    public List< ReportProfit> getProfit_loss(String begin,String end) {
         ReportProfitDao reportdao = new  ReportProfitDao();
         return reportdao.getProfit_loss(begin, end);
    }
}