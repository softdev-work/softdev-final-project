/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Service;

import com.mycompany.d.coffee.Dao.IngredientDao;
import java.util.List;
import model.Ingredient;

/**
 *
 * @author Asus
 */
public class IngredientService {
    public Ingredient getByCode(int code){
        IngredientDao ingredientDao = new IngredientDao();
        return ingredientDao.get(code);
    }
    
    public List<Ingredient> getIngredient(){
        IngredientDao ingredientDao = new IngredientDao();
        return ingredientDao.getAll(" ING_CODE asc");
    }

    public Ingredient addNew(Ingredient editedIngredient) {
        IngredientDao ingredientDao = new IngredientDao();
        return ingredientDao.save(editedIngredient);
    }

    public Ingredient update(Ingredient editedIngredient) {
        IngredientDao ingredientDao = new IngredientDao();
        return ingredientDao.update(editedIngredient);
    }

    public int delete(Ingredient editedIngredient) {
        IngredientDao ingredientDao = new IngredientDao();
        return ingredientDao.delete(editedIngredient);
    }
}
