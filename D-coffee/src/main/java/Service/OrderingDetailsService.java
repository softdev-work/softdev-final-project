/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Service;

import com.mycompany.d.coffee.Dao.OrderingDetailsDao;
import java.util.List;
import model.OrderingDetails;

/**
 *
 * @author informatics
 */
public class OrderingDetailsService {
    public OrderingDetails getById(int id){
        OrderingDetailsDao orderingDetailsDao = new OrderingDetailsDao();
        return orderingDetailsDao.get(id);
    }
              
    public List<OrderingDetails> getOrderingDetails(){
        OrderingDetailsDao orderingDetailsDao = new OrderingDetailsDao();
        return orderingDetailsDao.getAll(" ORD_D_CODE asc");
    }

    public OrderingDetails addNew(OrderingDetails editedOrderingDetails) {
        OrderingDetailsDao orderingDetailsDao = new OrderingDetailsDao();
        return orderingDetailsDao.save(editedOrderingDetails);
    }

    public OrderingDetails update(OrderingDetails editedOrderingDetails) {
        OrderingDetailsDao orderingDetailsDao = new OrderingDetailsDao();
        return orderingDetailsDao.update(editedOrderingDetails);
    }

    public int OrderingDetails(OrderingDetails editedOrderingDetails) {
        OrderingDetailsDao orderingDetailsDao = new OrderingDetailsDao();
        return orderingDetailsDao.delete(editedOrderingDetails);
    }
    
    public int delete(OrderingDetails editedOrderingDetails) {
        OrderingDetailsDao orderingDetailsDao = new OrderingDetailsDao();
        return orderingDetailsDao.delete(editedOrderingDetails);
    }
}
