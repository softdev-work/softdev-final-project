/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Service;

import com.mycompany.d.coffee.Dao.CheckIngredientDao;
import java.util.List;
import model.CheckIngredient;

/**
 *
 * @author ASUS
 */
public class CheckIngredientService {
              
    public List<CheckIngredient> getCheckIngredients(){
        CheckIngredientDao checkIngredientDao = new CheckIngredientDao();
        return checkIngredientDao.getAll(" CKIGD_CODE asc");
    }

    public CheckIngredient addNew(CheckIngredient editedCheckIngredient) {
        CheckIngredientDao checkIngredientDao = new CheckIngredientDao();
        return checkIngredientDao.save(editedCheckIngredient);
    }

    public CheckIngredient update(CheckIngredient editedCheckIngredient) {
        CheckIngredientDao checkIngredientDao = new CheckIngredientDao();
        return checkIngredientDao.update(editedCheckIngredient);
    }

    public int delete(CheckIngredient editedCheckIngredient) {
        CheckIngredientDao checkIngredientDao = new CheckIngredientDao();
        return checkIngredientDao.delete(editedCheckIngredient);
    }
}
