/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Service;

import com.mycompany.d.coffee.Dao.EmployeeDao;
import java.util.List;
import model.Employee;

/**
 *
 * @author ASUS
 */
public class EmployeeService {

    public Employee getByCode(int code) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.get(code);
    }

    public Employee getCurrentEmployee() {
        // สมมติว่ามีการเก็บข้อมูลของพนักงงานที่เข้าสู่ระบบ
        int currentEmployeeCode = 123; // ให้นำรหัสพนักงงานที่เข้าสู่ระบบมาใช้

        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.get(currentEmployeeCode);
    }

    public List<Employee> getEmployees() {
        EmployeeDao EmployeeDao = new EmployeeDao();
        return EmployeeDao.getAll(" EMP_CODE asc");
    }

    public Employee addNew(Employee editedEmployee) {
        EmployeeDao EmployeeDao = new EmployeeDao();
        return EmployeeDao.save(editedEmployee);
    }

    public Employee update(Employee editedEmployee) {
        EmployeeDao EmployeeDao = new EmployeeDao();
        return EmployeeDao.update(editedEmployee);
    }

    public int delete(Employee editedEmployee) {
        EmployeeDao EmployeeDao = new EmployeeDao();
        return EmployeeDao.delete(editedEmployee);
    }

}
