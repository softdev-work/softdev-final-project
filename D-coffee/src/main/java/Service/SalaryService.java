/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Service;

import com.mycompany.d.coffee.Dao.SalaryDao;
import java.util.List;
import model.Salary;

/**
 *
 * @author peakk
 */
public class SalaryService {
    public Salary getByCode(int code){
        SalaryDao branchdao = new SalaryDao();
        return branchdao.get(code);
    }
              
    public List<Salary> getSalary(){
        SalaryDao SalaryDao = new SalaryDao();
        return SalaryDao.getAll(" PS_CODE asc");
    }

    public Salary addNew(Salary editedSalary) {
        SalaryDao SalaryDao = new SalaryDao();
        return SalaryDao.save(editedSalary);
    }

    public Salary update(Salary editedSalary) {
        SalaryDao SalaryDao = new SalaryDao();
        return SalaryDao.update(editedSalary);
    }

    public int delete(Salary editedSalary) {
        SalaryDao SalaryDao = new SalaryDao();
        return SalaryDao.delete(editedSalary);
    }
}
