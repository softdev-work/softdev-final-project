/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Service;

import com.mycompany.d.coffee.Dao.CheckWorkDao;
import java.util.List;
import model.CheckWork;

/**
 *
 * @author sTCFILMs
 */
public class CheckWorkService {
    public CheckWork getByCode(int code) {
        CheckWorkDao checkWorkDao = new CheckWorkDao();
        return checkWorkDao.get(code);
    }

    public List<CheckWork> getCheckWork() {
        CheckWorkDao checkWorkDao = new CheckWorkDao();
        return checkWorkDao.getAll(" CW_CODE asc");
    }

    public CheckWork addNew(CheckWork editedCheckWork) {
        CheckWorkDao checkWorkDao = new CheckWorkDao();
        return checkWorkDao.save(editedCheckWork);
    }

    public CheckWork update(CheckWork editedCheckWork) {
        CheckWorkDao checkWorkDao = new CheckWorkDao();
        return checkWorkDao.update(editedCheckWork);
    }

    public int delete(CheckWork editedCheckWork) {
        CheckWorkDao checkWorkDao = new CheckWorkDao();
        return checkWorkDao.delete(editedCheckWork);
    }
}
