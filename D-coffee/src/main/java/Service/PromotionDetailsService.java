/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Service;

import com.mycompany.d.coffee.Dao.PromotionDetailsDao;
import java.util.List;
import model.PromotionDetails;

/**
 *
 * @author informatics
 */
public class PromotionDetailsService {
    public PromotionDetails getById(int id){
        PromotionDetailsDao promotionDetailsDao = new PromotionDetailsDao();
        return promotionDetailsDao.get(id);
    }
              
    public List<PromotionDetails> getPromotions(){
        PromotionDetailsDao promotionDetailsDao = new PromotionDetailsDao();
        return promotionDetailsDao.getAll(" PMT_D_CODE asc");
    }

    public PromotionDetails addNew(PromotionDetails editedPromotionDetails) {
        PromotionDetailsDao promotionDetailsDao = new PromotionDetailsDao();
        return promotionDetailsDao.save(editedPromotionDetails);
    }

    public PromotionDetails update(PromotionDetails editedPromotionDetails) {
        PromotionDetailsDao promotionDetailsDao = new PromotionDetailsDao();
        return promotionDetailsDao.update(editedPromotionDetails);
    }

    public int PromotionDetails(PromotionDetails editedPromotionDetails) {
        PromotionDetailsDao promotionDetailsDao = new PromotionDetailsDao();
        return promotionDetailsDao.delete(editedPromotionDetails);
    }
}
