/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Service;

import com.mycompany.d.coffee.Dao.CustomerDao;
import java.util.List;
import model.Customer;

/**
 *
 * @author ASUS
 */
public class CustomerService {
    public Customer getByCode(int code){
        CustomerDao customerDao = new CustomerDao();
        return customerDao.get(code);
    }
              
    public List<Customer> getCustomer(){
        CustomerDao CustomerDao = new CustomerDao();
        return CustomerDao.getAll(" CTM_CODE asc");
    }

    public Customer addNew(Customer editedCustomer) {
        CustomerDao CustomerDao = new CustomerDao();
        return CustomerDao.save(editedCustomer);
    }

    public Customer update(Customer editedCustomer) {
        CustomerDao CustomerDao = new CustomerDao();
        return CustomerDao.update(editedCustomer);
    }

    public int delete(Customer editedCustomer) {
        CustomerDao CustomerDao = new CustomerDao();
        return CustomerDao.delete(editedCustomer);
    }
}
