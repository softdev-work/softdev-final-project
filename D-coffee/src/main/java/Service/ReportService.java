/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Service;

import com.mycompany.d.coffee.Dao.ReportDao;
import java.util.List;
import model.Report;

/**
 *
 * @author peakk
 */
public class ReportService {
    public List<Report> getProductByAmount() {
        ReportDao productdao = new ReportDao();
        return productdao.getProductByAmount(10);
    }
    public List<Report> getProductByAmount(String begin,String end) {
        ReportDao productdao = new ReportDao();
        return productdao.getProductByAmount(begin,end,10);
    }
}
