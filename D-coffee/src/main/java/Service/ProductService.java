/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Service;

import com.mycompany.d.coffee.Dao.ProductDao;
import java.util.ArrayList;
import java.util.List;
import model.Product;

/**
 *
 * @author ASUS
 */
public class ProductService {

    public Product getByCode(int code) {
        ProductDao productDao = new ProductDao();
        return productDao.get(code);
    }

    public List<Product> getProduct() {
        ProductDao productDao = new ProductDao();
        return productDao.getAll(" PD_CODE asc");
    }

    public Product addNew(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.save(editedProduct);
    }

    public Product update(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.update(editedProduct);
    }

    public int delete(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.delete(editedProduct);
    }
        private final ProductDao productdao = new ProductDao();
    public ArrayList<Product> getProductsOderByname(){
        return (ArrayList<Product>) productdao.getAll(" PD_NAME ASC");
    }
}
