/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Thanaphat Ketsani(Wave)
 */
public class Ingredient {
    private int code;
    private String ingName;
    private float ingPrice;
    private String ingType;
    private int ingQoh;
    private int ingMin;
    private ArrayList<IngredientDetail> ingredientDetails = new ArrayList<IngredientDetail>();

    public Ingredient(int code, String ingName, float ingPrice, String ingType, int ingQoh, int ingMin) {
        this.code = code;
        this.ingName = ingName;
        this.ingPrice = ingPrice;
        this.ingType = ingType;
        this.ingQoh = ingQoh;
        this.ingMin = ingMin;
    }

    public Ingredient(String ingName, float ingPrice, String ingType, int ingQoh, int ingMin) {
        this.code = -1;
        this.ingName = ingName;
        this.ingPrice = ingPrice;
        this.ingType = ingType;
        this.ingQoh = ingQoh;
        this.ingMin = ingMin;
    }
    
    public Ingredient() {
        this.code = -1;
        this.ingName = "";
        this.ingPrice = 0;
        this.ingType = "";
        this.ingQoh = 0;
        this.ingMin = 0;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getIngName() {
        return ingName;
    }

    public void setIngName(String ingName) {
        this.ingName = ingName;
    }

    public float getIngPrice() {
        return ingPrice;
    }

    public void setIngPrice(float ingPrice) {
        this.ingPrice = ingPrice;
    }

    public String getIngType() {
        return ingType;
    }

    public void setIngType(String ingType) {
        this.ingType = ingType;
    }

    public int getIngQoh() {
        return ingQoh;
    }

    public void setIngQoh(int ingQoh) {
        this.ingQoh = ingQoh;
    }

    public int getIngMin() {
        return ingMin;
    }

    public void setIngMin(int ingMin) {
        this.ingMin = ingMin;
    }
    
     public ArrayList<IngredientDetail> getIngredientDetails() {
        return ingredientDetails;
    }

    public void setIngredientDetails(ArrayList ingredientDetails) {
        this.ingredientDetails = ingredientDetails;
    }

    @Override
    public String toString() {
        return "Ingredient{" + "code=" + code + ", ingName=" + ingName + ", ingPrice=" + ingPrice + ", ingType=" + ingType + ", ingQoh=" + ingQoh + ", ingMin=" + ingMin + '}';
    }
    
    public float calDamage(int exp,int dp,float price){
        return (exp+dp)*price;
    }
    
    public void addIngredientDetail(IngredientDetail ingredientDetail){
        ingredientDetails.add(ingredientDetail);
    }
    
    public void addIngredientDetail(Ingredient ingredient, int qty){
        IngredientDetail rd = new IngredientDetail( ingredient.getCode(), ingredient.getIngName() ,ingredient.getIngQoh(), 0, 0, ingredient.getIngPrice(),0,ingredient.getCode(),-1);
        ingredientDetails.add(rd);
    }
    
    public void delIngredientDetail(IngredientDetail ingredientDetail){
        ingredientDetails.remove(ingredientDetail);
    }
    
    public static Ingredient fromRS(ResultSet rs) {
        Ingredient ingredient = new Ingredient();
        try {
            ingredient.setCode(rs.getInt("ING_CODE"));
            ingredient.setIngName(rs.getString("ING_NAME"));
            ingredient.setIngPrice(rs.getFloat("ING_PRICE"));
            ingredient.setIngType(rs.getString("ING_TYPE"));
            ingredient.setIngQoh(rs.getInt("ING_QOH"));
            ingredient.setIngMin(rs.getInt("ING_MIN"));
        } catch (SQLException ex) {
            Logger.getLogger(Ingredient.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return ingredient;
    }
}