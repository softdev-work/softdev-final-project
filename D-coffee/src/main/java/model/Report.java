/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author peakk
 */
public class Report {
    private int code;
    private String name;
    private int total;

    public Report(int code, String name, int total) {
        this.code = code;
        this.name = name;
        this.total = total;
    }

    public Report(String name, int total) {
        this.code = -1;
        this.name = name;
        this.total = total;
    }

    public Report() {
        this(-1,"",0);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "ProductReport{" + "code=" + code + ", name=" + name + ", total=" + total + '}';
    }
    
     public static Report fromRS(ResultSet rs) {
        Report obj = new Report();
        try {        
            obj.setCode(rs.getInt("PD_CODE"));
            obj.setName(rs.getString("PName"));
            obj.setTotal(rs.getInt("PRODUCTAMOUNT"));
            
           
            
        } catch (SQLException ex) {
            Logger.getLogger(Report.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    }
  
}
