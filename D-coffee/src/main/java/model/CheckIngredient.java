/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import com.mycompany.d.coffee.Dao.CustomerDao;
import com.mycompany.d.coffee.Dao.EmployeeDao;
import com.mycompany.d.coffee.Dao.IngredientDetailDao;
import java.util.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author werapan
 */
public class CheckIngredient {

    private static int code;
    private Date date;
    private int exp;
    private int dp;
    private int tdp;
    private int employee;
    private Employee empemployee;
     IngredientDetailDao ind = new IngredientDetailDao();

    public CheckIngredient(int code, Date date, int exp, int dp, int tdp, int employee) {
        this.code = code;
        this.date = date;
        this.exp = exp;
        this.dp = dp;
        this.tdp = tdp;
        this.employee = employee;
    }

    public CheckIngredient(Date date, int exp, int dp, int tdp, int employee) {
        this.code = -1;
        this.date = date;
        this.exp = exp;
        this.dp = dp;
        this.tdp = tdp;
        this.employee = employee;
    }

    public CheckIngredient(int exp, int dp, int tdp, int employee) {
        this.code = -1;
        this.date = null;
        this.exp = exp;
        this.dp = dp;
        this.tdp = tdp;
        this.employee = employee;
    }
    
    public CheckIngredient(int code,int exp, int dp, int tdp, int employee) {
        this.code = code;
        this.date = null;
        this.exp = exp;
        this.dp = dp;
        this.tdp = tdp;
        this.employee = employee;
    }

    public CheckIngredient(int code,int employee) {
        this.code = code;
        this.date = null;
        this.exp = 0;
        this.dp = 0;
        this.tdp = 0;
        this.employee = employee;
    }

    public CheckIngredient() {
        this.code = -2;
        this.date = null;
        this.exp = 0;
        this.dp = 0;
        this.tdp = 0;
        this.employee = 0;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getExp() {
        int sum2 = 0;
        for (IngredientDetail ingd : ind.getAll()) {
            if (ingd.getCkigdCode() == getCode()) {
                sum2 += ingd.getExp();
            }
        }

        return sum2;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public int getDp() {
        int sum = 0;
        for (IngredientDetail ingd : ind.getAll()) {
            if (ingd.getCkigdCode() == getCode()) {
                sum += ingd.getDp();
            }
        }
        return sum;
    }

    public void setDp(int dp) {
        this.dp = dp;
    }

    public int getTdp() {
        int sum = 0;
        IngredientDetailDao ind = new IngredientDetailDao();
        for (IngredientDetail ingd : ind.getAll()) {
            if (ingd.getCkigdCode() == getCode()) {
                sum += ingd.getDamageValue();
            }
        }
        return sum;
    }

    public void setTdp(int tdp) {
        this.tdp = this.dp + this.exp;
    }

    public int getEmployee() {
        return employee;
    }

    public void setEmployee(int employee) {
        this.employee = employee;
    }

    public Employee getEmpemployee() {
        return empemployee;
    }

    public void setEmpemployee(Employee empemployee) {
        this.empemployee = empemployee;
    }

    public void setEmployee(Employee employee) {
        this.empemployee = employee;
        this.employee = employee.getCode();
    }

    @Override
    public String toString() {
        return "CheckIngredient{" + "code=" + code + ", date=" + date + ", exp=" + exp + ", dp=" + dp + ", tdp=" + tdp + ", employee=" + employee + ", empemployee=" + empemployee + '}';
    }

    public static CheckIngredient fromRS(ResultSet rs) throws ParseException {
        CheckIngredient checkIngredient = new CheckIngredient();
        try {
            checkIngredient.setCode(rs.getInt("CKIGD_CODE"));
            checkIngredient.setDate(rs.getTimestamp("CKIGD_DATE"));
            checkIngredient.setExp(rs.getInt("CKIGD_EXP"));
            checkIngredient.setDp(rs.getInt("CKIGD_DAMAGE_PIECES"));
            checkIngredient.setTdp(rs.getInt("CKIGD_TPD"));
            checkIngredient.setEmployee(rs.getInt("EMP_CODE"));

            EmployeeDao employeeDao = new EmployeeDao();
            Employee employee = employeeDao.get(checkIngredient.getEmployee());
            checkIngredient.setEmpemployee(employee);
        } catch (SQLException ex) {
            Logger.getLogger(CheckIngredient.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkIngredient;
    }
}
