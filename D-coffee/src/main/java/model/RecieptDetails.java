/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author werapan
 */
public class RecieptDetails {

    private int code;
    private String productName;
    private int productAmount;
    private float unitPrice;
    private float totalPrice;
    private float discount;
    private float netPrice;
    private int receiptCode;
    private int productCode;
    private int promotionDetialCode;

    public RecieptDetails(int code, String productName, int productAmount, float unitPrice, float totalPrice, float discount, float netPrice, int receiptCode, int productCode, int promotionDetialCode) {
        this.code = code;
        this.productName = productName;
        this.productAmount = productAmount;
        this.unitPrice = unitPrice;
        this.totalPrice = totalPrice;
        this.discount = discount;
        this.netPrice = netPrice;
        this.receiptCode = receiptCode;
        this.productCode = productCode;
        this.promotionDetialCode = promotionDetialCode;
    }

    public RecieptDetails(String productName, int productAmount, float unitPrice, float totalPrice , float discount, float netPrice, int receiptCode, int productCode, int promotionDetialCode) {
        this.code = -1;
        this.productName = productName;
        this.productAmount = productAmount;
        this.unitPrice = unitPrice;
        this.totalPrice = totalPrice;
        this.discount = discount;
        this.netPrice = netPrice;
        this.receiptCode = receiptCode;
        this.productCode = productCode;
        this.promotionDetialCode = promotionDetialCode;
    }

    public RecieptDetails() {
        this.code = -1;
        this.productName = "";
        this.productAmount = 0;
        this.unitPrice = 0;
        this.totalPrice = 0;
        this.discount = 0;
        this.netPrice = 0;
        this.receiptCode = 0;
        this.productCode = 0;
        this.promotionDetialCode = 0;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getProductAmount() {
        return productAmount;
    }

    public void setProductAmount(int productAmount) {
        this.productAmount = productAmount;
    }

    public float getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(float unitPrice) {
        this.unitPrice = unitPrice;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public float getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(float netPrice) {
        this.netPrice = netPrice;
    }

    public int getReceiptCode() {
        return receiptCode;
    }

    public void setReceiptCode(int receiptCode) {
        this.receiptCode = receiptCode;
    }

    public int getProductCode() {
        return productCode;
    }

    public void setProductCode(int productCode) {
        this.productCode = productCode;
    }

    public int getPromotionDetialCode() {
        return promotionDetialCode;
    }

    public void setPromotionDetialCode(int promotionDetialCode) {
        this.promotionDetialCode = promotionDetialCode;
    }


    

    @Override
    public String toString() {
        return "RecieptDetails{" + "code=" + code + ", productName=" + productName + ", productAmount=" + productAmount + ", unitPrice=" + unitPrice + ", totalPrice=" + totalPrice + ", discount=" + discount + ", netPrice=" + netPrice + ", receiptCode=" + receiptCode + ", productCode=" + productCode + ", promotionDetialCode=" + promotionDetialCode + '}';
    }

    public static RecieptDetails fromRS(ResultSet rs) {
        RecieptDetails recipet = new RecieptDetails();
        try {
            recipet.setCode(rs.getInt("RC_D_CODE"));
            recipet.setProductName(rs.getString("RC_D_PD_NAME"));
            recipet.setProductAmount(rs.getInt("RC_D_PD_AMOUNT"));
            recipet.setTotalPrice(rs.getFloat("RC_D_TOTAL_PRICE"));
            recipet.setDiscount(rs.getFloat("RC_D_DISCOUNT"));
            recipet.setNetPrice(rs.getFloat("RC_D_NET_PRICE"));
            recipet.setReceiptCode(rs.getInt("RECE_CODE"));
            recipet.setProductCode(rs.getInt("PD_CODE"));
            recipet.setPromotionDetialCode(rs.getInt("PMT_D_CODE"));

//            CustomerDao customerDao = new CustomerDao();
//            Customer customer = customerDao.get(recipet.getCode());
//           
//            EmployeeDao employeeDao = new EmployeeDao();
//            Employee employee = employeeDao.get(recipet.getCode());
////            
//            BranchDao branchDao = new BranchDao();
//            Branch branch = branchDao.get(recipet.getCode());
////            
////            
//            PromotionDao promotionDao = new PromotionDao();
//            Promotion promotion = promotionDao.get(recipet.getCode());
//            
//            recipet.setBrbranch(branch);
//            recipet.setPmpmtCode(promotion);
//            recipet.setEmpemployee(employee);
//            recipet.setEmpemployee(employee);
//            recipet.setCuscustomer(customer);
        } catch (SQLException ex) {
            Logger.getLogger(RecieptDetails.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return recipet;
    }

    void add(RecieptDetails recieptDetails) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    void remove(RecieptDetails recieptDetails) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
