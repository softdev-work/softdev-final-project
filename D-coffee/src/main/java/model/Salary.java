/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
   
/**
 *
 * @author peakk
 */
public class Salary {
 
    private int code;
    private Date date;
    private String status;
    private float base;
    private int employee;

    public Salary(int code, String status, float base, int employee) {
        this.code = code;
        this.date = null;
        this.status = status;
        this.base = base;
        this.employee = employee;
    }

    public Salary(String status, float base, int employee) {
        this.code = -1;
        this.date = null;
        this.status = status;
        this.base = base;
        this.employee = employee;
    }

    public Salary() {
        this.code = -1;
        this.date = null;
        this.status = "";
        this.base = 0;
        this.employee = 0;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public float getBase() {
        return base;
    }

    public void setBase(float base) {
        this.base = base;
    }

    public int getEmployee() {
        return employee;
    }

    public void setEmployee(int employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "Salary{" + "code=" + code + ", date=" + date + ", status=" + status + ", base=" + base + ", employee=" + employee + '}';
    }

    public static Salary fromRS(ResultSet rs) {
        Salary salary = new Salary();
        try {        
            salary.setCode(rs.getInt("PS_CODE"));
            salary.setDate(rs.getTimestamp("PS_PAYMENT_DATETIME"));
            salary.setStatus(rs.getString("PS_Status"));
            salary.setBase(rs.getFloat("PS_BASE_SALARY"));
            salary.setEmployee(rs.getInt("EMP_CODE"));
            
        } catch (SQLException ex) {
            Logger.getLogger(Salary.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return salary;
    }
  
    
    
}
