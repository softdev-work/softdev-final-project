/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import com.mycompany.d.coffee.Dao.BranchDao;
import com.mycompany.d.coffee.Dao.CustomerDao;
import com.mycompany.d.coffee.Dao.EmployeeDao;
import com.mycompany.d.coffee.Dao.PromotionDao;
import java.util.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author werapan
 */
public class Reciept {

    private int code;
    private Date date;
    private float total;
    private float discount;
    private float change;
    private float netPrice;
    private float payment;
    private int employee;
    private int customer;
    private int branch;
    private int pmtCode;
    private Employee empemployee;
    private Customer cuscustomer;
    private Branch brbranch;
    private Promotion pmpmtCode;
    private ArrayList<RecieptDetails> recieptDetails = new ArrayList<RecieptDetails>();
    private int totalQty;

    public Reciept(int code, float total, float discount, float change, float netPrice, float payment, int employee, int customer, int branch, int pmtCode) {
        this.code = code;
        this.date = null;
        this.total = total;
        this.discount = discount;
        this.change = change;
        this.netPrice = netPrice;
        this.payment = payment;
        this.employee = employee;
        this.customer = customer;
        this.branch = branch;
        this.pmtCode = pmtCode;
    }

    public Reciept(float total, float discount, float change, float netPrice, float payment, int employee, int customer, int branch, int pmtCode) {
        this.code = -1;
        this.date = null;
        this.total = total;
        this.discount = discount;
        this.change = change;
        this.netPrice = netPrice;
        this.payment = payment;
        this.employee = employee;
        this.customer = customer;
        this.branch = branch;
        this.pmtCode = pmtCode;
    }

    public ArrayList<RecieptDetails> getRecieptDetails() {
        return recieptDetails;
    }

    public Reciept() {
        this.code = -1;
        this.date = null;
        this.total = 0;
        this.discount = 0;
        this.change = 0;
        this.netPrice = 0;
        this.payment = 0;
        this.employee = 0;
        this.customer = 0;
        this.branch = 0;
        this.pmtCode = 0;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public float getChange() {
        return change;
    }

    public void setChange(float change) {
        this.change = change;
    }

    public float getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(float netPrice) {
        this.netPrice = netPrice;
    }

    public float getPayment() {
        return payment;
    }

    public void setPayment(float payment) {
        this.payment = payment;
    }

    public int getEmployee() {
        return employee;
    }

    public void setEmployee(int employee) {
        this.employee = employee;
    }

    public int getCustomer() {
        return customer;
    }

    public void setCustomer(int customer) {
        this.customer = customer;
    }

    public int getBranch() {
        return branch;
    }

    public void setBranch(int branch) {
        this.branch = branch;
    }

    public int getPmtCode() {
        return pmtCode;
    }

    public void setPmtCode(int pmtCode) {
        this.pmtCode = pmtCode;
    }

    public Employee getEmpemployee() {
        return empemployee;
    }

    public void setEmpemployee(Employee empemployee) {
        this.empemployee = empemployee;
    }

    public Customer getCuscustomer() {
        return cuscustomer;
    }

    public void setCuscustomer(Customer cuscustomer) {
        this.cuscustomer = cuscustomer;
    }

    public Branch getBrbranch() {
        return brbranch;
    }

    public void setBrbranch(Branch brbranch) {
        this.brbranch = brbranch;
    }

    public Promotion getPmpmtCode() {
        return pmpmtCode;
    }

    public void setPmpmtCode(Promotion pmpmtCode) {
        this.pmpmtCode = pmpmtCode;
    }

    @Override
    public String toString() {
        return "Reciept{" + "code=" + code + ", date=" + date + ", total=" + total + ", discount=" + discount + ", change=" + change + ", netPrice=" + netPrice + ", payment=" + payment + ", employee=" + employee + ", customer=" + customer + ", branch=" + branch + ", pmtCode=" + pmtCode + '}';
    }

    public static Reciept fromRS(ResultSet rs) {
        Reciept recipet = new Reciept();
        try {
            recipet.setCode(rs.getInt("RECE_CODE"));
            recipet.setDate(rs.getTimestamp("RECE_DATE"));
            recipet.setTotal(rs.getFloat("RECE_TOTAL"));
            recipet.setDiscount(rs.getFloat("RECE_DISCOUNT"));
            recipet.setChange(rs.getFloat("RECE_CHANGE"));
            recipet.setNetPrice(rs.getFloat("RECE_NET_PRICE"));
            recipet.setPayment(rs.getFloat("RECE_PAYMENT"));
            recipet.setEmployee(rs.getInt("EMP_CODE"));
            recipet.setCustomer(rs.getInt("CTM_CODE"));
            recipet.setBranch(rs.getInt("BRC_CODE"));
            recipet.setPmtCode(rs.getInt("PMT_CODE"));

//            CustomerDao customerDao = new CustomerDao();
//            Customer customer = customerDao.get(recipet.getCode());
//           
//            EmployeeDao employeeDao = new EmployeeDao();
//            Employee employee = employeeDao.get(recipet.getCode());
////            
//            BranchDao branchDao = new BranchDao();
//            Branch branch = branchDao.get(recipet.getCode());
////            
////            
//            PromotionDao promotionDao = new PromotionDao();
//            Promotion promotion = promotionDao.get(recipet.getCode());
//            
//            recipet.setBrbranch(branch);
//            recipet.setPmpmtCode(promotion);
//            recipet.setEmpemployee(employee);
//            recipet.setEmpemployee(employee);
//            recipet.setCuscustomer(customer);
        } catch (SQLException ex) {
            Logger.getLogger(Reciept.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return recipet;
    }

    public void addRecieptDetails(Product product, int qty) {
        RecieptDetails rd = new RecieptDetails(product.getCode(), product.getName(),
                qty, product.getPrice(),qty*product.getPrice(),
                0, 0, 0, 0, 0);
        recieptDetails.add(rd);
        calculateTotal();
    }
        public void calculateTotal() {
        int totalQty = 0;
        float total = 0.0f;
        for (RecieptDetails rd : recieptDetails) {
            total += rd.getTotalPrice();
            totalQty += rd.getUnitPrice();
        }
        this.totalQty = totalQty;
        this.total = total;
        
    }

}
