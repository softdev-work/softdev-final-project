/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author peakk
 */
public class ReportProfit {
    private float  income;
    private float  expenses;
    private float result;

    public ReportProfit(float income, float expenses, float result) {
        this.income = income;
        this.expenses = expenses;
        this.result = result;
    }
 

    public ReportProfit() {
        this(0,0,0);
    }

    public float getIncome() {
        return income;
    }

    public void setIncome(float income) {
        this.income = income;
    }

    public float getExpenses() {
        return expenses;
    }

    public void setExpenses(float expenses) {
        this.expenses = expenses;
    }

    public float getResult() {
        return result;
    }

    public void setResult(float result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "ReportProfit{" + "income=" + income + ", expenses=" + expenses + ", result=" + result + '}';
    }

    
   
    public static ReportProfit fromRS(ResultSet rs) {
        ReportProfit obj = new ReportProfit();
        try {        
            obj.setIncome(rs.getFloat("INCOME"));
            obj.setExpenses(rs.getFloat("EXPENSES"));
            obj.setResult(rs.getFloat("RESULT"));
        } catch (SQLException ex) {
            Logger.getLogger(ReportProfit.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    }
}
