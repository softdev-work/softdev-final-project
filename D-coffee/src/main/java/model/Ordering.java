/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import Service.OrderingService;
import com.mycompany.d.coffee.Dao.OrderingDao;
import com.mycompany.d.coffee.Dao.OrderingDetailsDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author WIN10
 */
public class Ordering {
    private int ordCode;
    private Date ordDate;
    private int ordAmount;
    private float ordPrice;
    private String ordVendor;
    private Date ordPayDate;
    private float ordDiscount;
    private int employee;
    private Employee emp;
    OrderingDetails ordD = new OrderingDetails();

    public Ordering(int ordCode, Date ordDate, int ordAmount, float ordPrice, String ordVendor, Date ordPayDate, float ordDiscount, int employee) {
        this.ordCode = ordCode;
        this.ordDate = ordDate;
        this.ordAmount = ordAmount;
        this.ordPrice = ordPrice;
        this.ordVendor = ordVendor;
        this.ordPayDate = ordPayDate;
        this.ordDiscount = ordDiscount;
        this.employee = employee;
    }
    
    public Ordering( Date ordDate, int ordAmount, float ordPrice, String ordVendor, Date ordPayDate, float ordDiscount, int employee) {
        this.ordCode = -1;
        this.ordDate = ordDate;
        this.ordAmount = ordAmount;
        this.ordPrice = ordPrice;
        this.ordVendor = ordVendor;
        this.ordPayDate = ordPayDate;
        this.ordDiscount = ordDiscount;
        this.employee = employee;
    }
    
    public Ordering() {
        this.ordCode = -1;
        this.ordDate = null;
        this.ordAmount = 0;
        this.ordPrice = 0;
        this.ordVendor = null;
        this.ordPayDate = null;
        this.ordDiscount = 0;
        this.employee = 0;
    }

    public int getOrdCode() {
        return ordCode;
    }

    public void setOrdCode(int ordCode) {
        this.ordCode = ordCode;
    }

    public Date getOrdDate() {
        return ordDate;
    }

    public void setOrdDate(Date ordDate) {
        this.ordDate = ordDate;
    }

    public int getOrdAmount() {
        return ordAmount;
    }

    public void setOrdAmount(int ordAmount) {
        OrderingDetailsDao ord = new OrderingDetailsDao();
        for(OrderingDetails ordD: ord.getAll()){
            if(ordD.getOrdCode() == ordCode){
                ordAmount+= ordD.getAmount();
            }
        }
        this.ordAmount = ordAmount;
    }

    public float getOrdPrice() {
        return ordPrice;
    }

    public void setOrdPrice(float ordPrice) {
        OrderingDetailsDao ord = new OrderingDetailsDao();
        for(OrderingDetails ordD: ord.getAll()){
            if(ordD.getOrdCode() == this.ordCode){
                ordPrice+= ordD.getNetPrice();
            }
        }
        this.ordPrice = ordPrice;
    }

    public String getOrdVendor() {
        return ordVendor;
    }

    public void setOrdVendor(String ordVendor) {
        this.ordVendor = ordVendor;
    }

    public Date getOrdPayDate() {
        return ordPayDate;
    }

    public void setOrdPayDate(Date ordPayDate) {
        this.ordPayDate = ordPayDate;
    }

    public float getOrdDiscount() {
        return ordDiscount;
    }

    public void setOrdDiscount(float ordDiscount) {
        OrderingDetailsDao ord = new OrderingDetailsDao();
        for(OrderingDetails ordD: ord.getAll()){
            if(ordD.getOrdCode() == this.ordCode){
                ordDiscount+= ordD.getDiscount();
            }
        }
        this.ordDiscount = ordDiscount;
    }

    public int getEmployee() {
        return employee;
    }

    public void setEmployee(int employee) {
        this.employee = employee;
    }

    public Employee getEmp() {
        return emp;
    }

    public void setEmp(Employee emp) {
        this.emp = emp;
    }
    
    

    @Override
    public String toString() {
        return "Ordering{" + "ordCode=" + ordCode + ", ordDate=" + ordDate + ", ordAmount=" + ordAmount + ", ordPrice=" + ordPrice + ", ordVendor=" + ordVendor  + ", ordPayDate=" + ordPayDate + ", ordDiscount=" + ordDiscount + ", employee=" + employee + '}';
    }
    
    
    public static Ordering fromRS(ResultSet rs) {
        Ordering ordering = new Ordering();
        try{
            ordering.setOrdCode(rs.getInt("ORD_CODE"));
            ordering.setOrdDate(rs.getTimestamp("ORD_DATE"));
            ordering.setOrdAmount(rs.getInt("ORD_AMOUNT"));
            ordering.setOrdPrice(rs.getFloat("ORD_PRICE"));
            ordering.setOrdVendor(rs.getString("ORD_VENDOR"));
            ordering.setOrdPayDate(rs.getTimestamp("ORD_PAYDATE"));
            ordering.setOrdDiscount(rs.getFloat("ORD_DISCOUNT"));
            ordering.setEmployee(rs.getInt("EMP_CODE"));
            
        }catch (SQLException ex) {
            Logger.getLogger(Ordering.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return ordering;
    }
}
