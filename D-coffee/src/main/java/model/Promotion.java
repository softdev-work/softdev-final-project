/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import com.mycompany.d.coffee.Dao.RecieptDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sTCFILMs
 */
public class Promotion {

    private int code;
    private String name;
    private Date startdate;
    private Date enddate;
    private String status;
    private int recieptId;
    private Reciept reciept;

    public Promotion(int code, String name, Date startdate, Date enddate, String status, int recieptId, Reciept reciept) {
        this.code = code;
        this.name = name;
        this.startdate = startdate;
        this.enddate = enddate;
        this.status = status;
        this.recieptId = recieptId;
        this.reciept = reciept;
    }

    public Promotion(int code, String name, Date startdate, Date enddate, String status, int recieptId) {
        this.code = code;
        this.name = name;
        this.startdate = startdate;
        this.enddate = enddate;
        this.status = status;
        this.recieptId = recieptId;
    }

    public Promotion(String name, Date startdate, Date enddate, String status, int recieptId) {
        this.code = -1;
        this.name = name;
        this.startdate = startdate;
        this.enddate = enddate;
        this.status = status;
        this.recieptId = recieptId;
    }
    
    public Promotion(String name, String status, int recieptId) {
        this.code = -1;
        this.name = name;
        this.startdate = null;
        this.enddate = null;
        this.status = status;
        this.recieptId = recieptId;
    }
    
    public Promotion() {
        this.code = -1;
        this.name = "";
        this.startdate = null;
        this.enddate = null;
        this.status = "";
        this.recieptId = 0;
    }
    
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getRecieptId() {
        return recieptId;
    }

    public void setRecieptId(int recieptId) {
        this.recieptId = recieptId;
    }

    public Reciept getReciept() {
        return reciept;
    }

    public void setReciept(Reciept reciept) {
        this.reciept = reciept;
        this.recieptId = reciept.getCode();
    }

    @Override
    public String toString() {
        return "Promotion{" + "code=" + code + ", name=" + name + ", startdate=" + startdate + ", enddate=" + enddate + ", status=" + status + ", recieptId=" + recieptId + '}';
    }

    
    
    
    public static Promotion fromRS(ResultSet rs) {
        Promotion promotion = new Promotion();
        try {
            promotion.setCode(rs.getInt("PMT_CODE"));
            promotion.setName(rs.getString("PMT_NAME"));
            promotion.setStartdate(rs.getTimestamp("PMT_START_DATE"));
            promotion.setEnddate(rs.getTimestamp("PMT_END_DATE"));
            promotion.setStatus(rs.getString("PMT_STATUS"));
            promotion.setRecieptId(rs.getInt("RECE_CODE"));   
            
           // RecieptDao recieptDao = new RecieptDao();
            //Reciept reciept = recieptDao.get(promotion.getCode());
        } catch (SQLException ex) {
            Logger.getLogger(Promotion.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return promotion;
    }
}

