/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sTCFILMs
 */
public class Product {

    private int code;
    private String name;
    private float price;



    public Product(int code, String name, float price) {
        this.code = code;
        this.name = name;
        this.price = price;
    }

    public Product(String name, float price) {
        this.code = -1;
        this.name = name;
        this.price = price;
    }

    public Product() {
        this.code = -1;
        this.name = "";
        this.price = 0;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" + "code=" + code + ", name=" + name + ", price=" + price + '}';
    }
    
    public static Product fromRS(ResultSet rs) {
        Product product = new Product();
        try {
            product.setCode(rs.getInt("PD_CODE"));
            product.setName(rs.getString("PD_NAME"));
            product.setPrice(rs.getFloat("PD_PRICE"));
        } catch (SQLException ex) {
            Logger.getLogger(Product.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return product;
    }

}
