/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Asus
 */
public class IngredientDetail {
    private int code;
    private String name;
    private int qoh;
    private int exp;
    private int dp;
    private float unitPrice;
    private float damageValue;
    private int ingCode;
    private int ckigdCode;

    public IngredientDetail(int code, String name, int qoh, int exp, int dp, float unitPrice, float damageValue, int ingCode, int ckigdCode) {
        this.code = code;
        this.name = name;
        this.qoh = qoh;
        this.exp = exp;
        this.dp = dp;
        this.unitPrice = unitPrice;
        this.damageValue = damageValue;
        this.ingCode = ingCode;
        this.ckigdCode = ckigdCode;
    }
    
    public IngredientDetail(String name, int qoh, int exp, int dp, float unitPrice, float damageValue, int ingCode, int ckigdCode) {
        this.code = -1;
        this.name = name;
        this.qoh = qoh;
        this.exp = exp;
        this.dp = dp;
        this.unitPrice = unitPrice;
        this.damageValue = damageValue;
        this.ingCode = ingCode;
        this.ckigdCode = ckigdCode;
    }
    
    public IngredientDetail() {
        this.code = -2;
        this.name = "";
        this.qoh = 0;
        this.exp = 0;
        this.dp = 0;
        this.unitPrice = 0;
        this.damageValue = 0;
        this.ingCode = 0;
        this.ckigdCode = 0;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQoh() {
        return qoh;
    }

    public void setQoh(int qoh) {
        this.qoh = qoh;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public int getDp() {
        return dp;
    }

    public void setDp(int dp) {
        this.dp = dp;
    }

    public float getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(float unitPrice) {
        this.unitPrice = Float.parseFloat(String.format("%.2f", unitPrice));
    }

    public float getDamageValue() {
         float dm = (exp+dp)*unitPrice;
        this.damageValue = dm;
        return Float.parseFloat(String.format("%.2f", dm));
    }

    public void setDamageValue(float damageValue) {
        this.damageValue = Float.parseFloat(String.format("%.2f", damageValue));
    }
    
    

    public int getIngCode() {
        return ingCode;
    }

    public void setIngCode(int ingCode) {
        this.ingCode = ingCode;
    }

    public int getCkigdCode() {
        return ckigdCode;
    }

    public void setCkigdCode(int ckigdCode) {
        this.ckigdCode = ckigdCode;
    }

    @Override
    public String toString() {
        return "IngredientDetail{" + "code=" + code + ", name=" + name + ", qoh=" + qoh + ", exp=" + exp + ", dp=" + dp + ", unitPrice=" + unitPrice + ", damageValue=" + damageValue + ", ingCode=" + ingCode + ", ckigdCode=" + ckigdCode + '}';
    }
    
    
    public static IngredientDetail fromRS(ResultSet rs) {
        IngredientDetail ingredientDetail = new IngredientDetail();
        try {
            ingredientDetail.setCode(rs.getInt("IGD_D_CODE"));
            ingredientDetail.setName(rs.getString("IGD_D_NAME"));
            ingredientDetail.setQoh(rs.getInt("IGD_D_QOH"));
            ingredientDetail.setExp(rs.getInt("IGD_EXP_PIECES"));
            ingredientDetail.setDp(rs.getInt("IGD_D_DAMAGE_PIECES"));
            ingredientDetail.setUnitPrice(rs.getFloat("IGD_D_UNIT_PRICE"));
            ingredientDetail.setDamageValue(rs.getFloat("IGD_D_DAMAGE_VALUE"));
            ingredientDetail.setIngCode(rs.getInt("ING_CODE"));
            ingredientDetail.setCkigdCode(rs.getInt("CKIGD_CODE"));
        } catch (SQLException ex) {
            Logger.getLogger(IngredientDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return ingredientDetail;
    }
    
    
}