/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author werapan
 */
public class Customer {

    private int Code;
    private String Fname;
    private String Lname;
    private int Age;
    private String Phone;
    private String Address;
    private String Email;
    private int Ponit;
    

    public Customer(int Code, String Fname, String Lname, int Age, String Phone, String Address, String Email, int Ponit) {
        this.Code = Code;
        this.Fname = Fname;
        this.Lname = Lname;
        this.Age = Age;
        this.Phone = Phone;
        this.Address = Address;
        this.Email = Email;
        this.Ponit = Ponit;
    }
    
    public Customer(String Fname, String Lname, int Age, String Phone, String Address, String Email, int Ponit) {
        this.Code = -1;
        this.Fname = Fname;
        this.Lname = Lname;
        this.Age = Age;
        this.Phone = Phone;
        this.Address = Address;
        this.Email = Email;
        this.Ponit = Ponit;
    }
    
    
    public Customer() {
        this.Code = -1;
        this.Fname = "";
        this.Lname = "";
        this.Age = 0;
        this.Phone = "";
        this.Address = "";
        this.Email = "";
        this.Ponit = 0;
    }
    
    

    public int getCode() {
        return Code;
    }

    public void setCode(int Code) {
        this.Code = Code;
    }

    public String getFname() {
        return Fname;
    }

    public void setFname(String Fname) {
        this.Fname = Fname;
    }

    public String getLname() {
        return Lname;
    }

    public void setLname(String Lname) {
        this.Lname = Lname;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int Age) {
        this.Age = Age;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String Phone) {
        this.Phone = Phone;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public int getPonit() {
        return Ponit;
    }

    public void setPonit(int Ponit) {
        this.Ponit = Ponit;
    }

    @Override
    public String toString() {
        return "CUSTOMER{" + "Code=" + Code + ", Fname=" + Fname + ", Lname=" + Lname + ", Age=" + Age + ", Phone=" + Phone + ", Address=" + Address + ", Email=" + Email + ", Ponit=" + Ponit + '}';
    }
    public static Customer fromRS(ResultSet rs) {
        Customer custome = new Customer();
        try {
            custome.setCode(rs.getInt("CTM_CODE"));
            custome.setFname(rs.getString("CTM_FNAME"));
            custome.setLname(rs.getString("CTM_LNAME"));
            custome.setAge(rs.getInt("CTM_AGE"));
            custome.setPhone(rs.getString("CTM_PHONE"));
            custome.setAddress(rs.getString("CTM_ADDRESS"));
            custome.setEmail(rs.getString("CTM_EMAIL"));
            custome.setPonit(rs.getInt("CTM_POINT"));
        } catch (SQLException ex) {
            Logger.getLogger(Customer.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return custome;
    }

//    public Object getFName() {
//        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
//    }
//
//    public int getLName() {
//        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
//    }
}
