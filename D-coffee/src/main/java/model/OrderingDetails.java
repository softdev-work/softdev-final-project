/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import com.mycompany.d.coffee.Dao.OrderingDetailsDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author WIN10
 */
public class OrderingDetails {
    private int Code;
    private String Name;
    private int Amount;
    private float unitPrice;
    private float totalPrice;
    private float Discount;
    private float netPrice;
    private int ordCode;
    private int ingCode;
    private Ordering ord;
    private Ingredient ing;

    public OrderingDetails(int Code, String Name, int Amount, float unitPrice, float totalPrice, float Discount, float netPrice, int ordCode, int ingCode, Ordering ord, Ingredient ing) {
        this.Code = Code;
        this.Name = Name;
        this.Amount = Amount;
        this.unitPrice = unitPrice;
        this.totalPrice = totalPrice;
        this.Discount = Discount;
        this.netPrice = netPrice;
        this.ordCode = ordCode;
        this.ingCode = ingCode;
        this.ord = ord;
        this.ing = ing;
    }

    public OrderingDetails(int Code, String Name, int Amount, float unitPrice, float totalPrice, float Discount, float netPrice, int ordCode, int ingCode) {
        this.Code = Code;
        this.Name = Name;
        this.Amount = Amount;
        this.unitPrice = unitPrice;
        this.totalPrice = totalPrice;
        this.Discount = Discount;
        this.netPrice = netPrice;
        this.ordCode = ordCode;
        this.ingCode = ingCode;
    }

    public OrderingDetails(String Name, int Amount, float unitPrice, float totalPrice, float Discount, float netPrice, int ordCode, int ingCode) {
        this.Code = -1;
        this.Name = Name;
        this.Amount = Amount;
        this.unitPrice = unitPrice;
        this.totalPrice = totalPrice;
        this.Discount = Discount;
        this.netPrice = netPrice;
        this.ordCode = ordCode;
        this.ingCode = ingCode;
    }

    public OrderingDetails() {
        this.Code = -1;
        this.Name = "";
        this.Amount = 0;
        this.unitPrice = 0;
        this.totalPrice = 0;
        this.Discount = 0;
        this.netPrice = 0;
        this.ordCode = 0;
        this.ingCode = 0;
    }

    public int getCode() {
        return Code;
    }

    public void setCode(int Code) {
        this.Code = Code;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getAmount() {
        return Amount;
    }

    public void setAmount(int Amount) {
        this.Amount = Amount;
    }

    public float getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(float unitPrice) {
        this.unitPrice = unitPrice;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = (this.Amount*this.unitPrice);
    }

    public float getDiscount() {
        return Discount;
    }

    public void setDiscount(float Discount) {
        this.Discount = Discount;
    }

    public float getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(float netPrice) {
        this.netPrice = (this.totalPrice)-this.Discount;
    }

    public int getOrdCode() {
        return ordCode;
    }

    public void setOrdCode(int ordCode) {
        this.ordCode = ordCode;
    }

    public int getIngCode() {
        return ingCode;
    }

    public void setIngCode(int ingCode) {
        this.ingCode = ingCode;
    }

    public Ordering getOrd() {
        return ord;
    }

    public void setOrd(Ordering ord) {
        this.ord = ord;
        this.ordCode = this.ord.getOrdCode();
    }

    public Ingredient getIng() {
        return ing;
    }

    public void setIng(Ingredient ing) {
        this.ing = ing;
        this.ingCode = this.ing.getCode();
    }

    @Override
    public String toString() {
        return "OrderingDetails{" + "Code=" + Code + ", Name=" + Name + ", Amount=" + Amount + ", unitPrice=" + unitPrice + ", totalPrice=" + totalPrice + ", Discount=" + Discount + ", netPrice=" + netPrice + ", ordCode=" + ordCode + ", ingCode=" + ingCode + '}';
    }

    
    
    
    public static OrderingDetails fromRS(ResultSet rs) {
        OrderingDetails orderingDetails = new OrderingDetails();
        try {
            orderingDetails.setCode(rs.getInt("ORD_D_CODE"));
            orderingDetails.setName(rs.getString("ORD_D_NAME"));
            orderingDetails.setAmount(rs.getInt("ORD_D_AMOUNT"));
            orderingDetails.setUnitPrice(rs.getFloat("ORD_D_UNIT_PRICE"));
            orderingDetails.setTotalPrice(rs.getFloat("ORD_D_TOTAL_PRICE"));
            orderingDetails.setDiscount(rs.getInt("ORD_D_DISCOUNT"));   
            orderingDetails.setNetPrice(rs.getInt("ORD_D_NETPRICE"));
            orderingDetails.setOrdCode(rs.getInt("ORD_Code"));
            orderingDetails.setIngCode(rs.getInt("ING_Code"));
           
        } catch (SQLException ex) {
            Logger.getLogger(OrderingDetails.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return orderingDetails;
    }
}
