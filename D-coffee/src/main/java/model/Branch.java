/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author peakk
 */
public class Branch {
    private int  code;
    private String name;
    private String adress;
    private int aoe;
    private String phone;

    public Branch(int code, String name, String adress, int aoe, String phone) {
        this.code = code;
        this.name = name;
        this.adress = adress;
        this.aoe = aoe;
        this.phone = phone;
    }

    public Branch(String name, String adress, int aoe, String phone) {
        this.code = -1;
        this.name = name;
        this.adress = adress;
        this.aoe = aoe;
        this.phone = phone;
    }

    public Branch() {
        this.code = -1;
        this.name = "";
        this.adress = "";
        this.aoe = 0;
        this.phone = "";
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public int getAoe() {
        return aoe;
    }

    public void setAoe(int aoe) {
        this.aoe = aoe;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Branch{" + "code=" + code + ", name=" + name + ", adress=" + adress + ", aoe=" + aoe + ", phone=" + phone + '}';
    }
     public static Branch fromRS(ResultSet rs) {
       Branch branch = new Branch();
        try {
            branch.setCode(rs.getInt("BRC_CODE"));
            branch.setName(rs.getString("BRC_NAME"));
            branch.setAdress(rs.getString("BRC_ADRESS"));
            branch.setAoe(rs.getInt("BRC_AOE"));
            branch.setPhone(rs.getString("BRC_PHONE"));
        } catch (SQLException ex) {
            Logger.getLogger(Branch.class.getName()).log(Level.SEVERE, null, ex);
            
        }
        return branch;
        
    }
}
