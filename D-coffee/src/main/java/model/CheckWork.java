/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import com.mycompany.d.coffee.Dao.EmployeeDao;
import java.util.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author werapan
 */
public class CheckWork {

    private int code;
    private Date date;
    private String timein;
    private String timeout;
    private int employee;

    public CheckWork(int code, Date date, String timein, String timeout,int employee) {
        this.code = code;
        this.date = date;
        this.timein = timein;
        this.timeout = timeout;
        this.employee = employee;
    }

    public CheckWork( String timein, String timeout, int employee) {
        this.code = -1;
        this.date = null;
        this.timein = timein;
        this.timeout = timeout;
        this.employee = employee;
    }

    public CheckWork() {
        this.code = -1;
        this.date = null;
        this.timein = null;
        this.timeout = null;
        this.employee = 0;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTimein() {
        return timein;
    }

    public void setTimein(String timein) {
        this.timein = timein;
    }

    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }


    public int getEmployee() {
        return employee;
    }

    public void setEmployee(int employee) {
        this.employee = employee;
    }


    @Override
    public String toString() {
        return "CheckWork{" + "code=" + code + ", date=" + date + ", timein=" + timein + ", timeout=" + timeout + ", employee=" + employee + '}';
    }

    
    public static CheckWork fromRS(ResultSet rs) throws ParseException {
        CheckWork checkWork = new CheckWork();
        try {
            checkWork.setCode(rs.getInt("CW_CODE"));
            checkWork.setDate(rs.getTimestamp("CW_DATE"));
            checkWork.setTimein(rs.getString("CW_TIMEIN"));
            checkWork.setTimeout(rs.getString("CW_TIMEOUT"));
            checkWork.setEmployee(rs.getInt("EMP_CODE"));
            EmployeeDao employeeDao = new EmployeeDao();
        } catch (SQLException ex) {
            Logger.getLogger(CheckWork.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkWork;
    }
}
