/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author informatics
 */
public class PromotionDetails {
    private int code;
    private String condition;
    private float discount;
    private String name;
    private String status;
    private int productId;
    private int promotionId;
    private Product product;
    private Promotion promotion;

    public PromotionDetails(int code, String condition, float discount, String name, String status, int productId, int promotionId,  Product product, Promotion promotio ) {
        this.code = code;
        this.condition = condition;
        this.discount = discount;
        this.name = name;
        this.status = status;
        this.productId = productId;
        this.promotionId = promotionId;
        this.promotion = promotion;
        this.product = product;
    }

    public PromotionDetails(int code, String condition, float discount, String name, String status, int productId, int promotionId) {
        this.code = code;
        this.condition = condition;
        this.discount = discount;
        this.name = name;
        this.status = status;
        this.productId = productId;
        this.promotionId = promotionId;
    }

    public PromotionDetails(String condition, float discount, String name, String status, int productId, int promotionId) {
        this.code = -1;
        this.condition = condition;
        this.discount = discount;
        this.name = name;
        this.status = status;
        this.productId = productId;
        this.promotionId = promotionId;
    }
    
     public PromotionDetails() {
        this.code = -2;
        this.condition = "";
        this.discount = 0;
        this.name = "";
        this.status = "";
        this.productId = 0;
        this.promotionId = 0;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(int promotionId) {
        this.promotionId = promotionId;
    }

    public Promotion getPromotion() {
        return promotion;
    }

    public void setPromotion(Promotion promotion) {
        this.promotion = promotion;
        this.promotionId = this.promotion.getCode();
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
        this.productId = product.getCode();
    }

    @Override
    public String toString() {
        return "PromotionDetails{" + "code=" + code + ", condition=" + condition + ", discount=" + discount + ", name=" + name + ", status=" + status + ", productId=" + productId + ", promotionId=" + promotionId + '}';
    }


    public static PromotionDetails fromRS(ResultSet rs) {
        PromotionDetails promotionDetails = new PromotionDetails();
        try {
            promotionDetails.setCode(rs.getInt("PMT_D_CODE"));
            promotionDetails.setCondition(rs.getString("PMT_D_CONDITION"));
            promotionDetails.setDiscount(rs.getFloat("PMT_T_DISCOUNT"));
            promotionDetails.setName(rs.getString("PMT_D_NAME"));
            promotionDetails.setStatus(rs.getString("PMT_D_STATUS"));
            promotionDetails.setProductId(rs.getInt("PD_CODE"));   
            promotionDetails.setPromotionId(rs.getInt("PMT_CODE"));
           
        } catch (SQLException ex) {
            Logger.getLogger(PromotionDetails.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return promotionDetails;
    }
}
