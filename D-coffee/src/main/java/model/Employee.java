/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author werapan
 */
public class Employee {

    private int Code;
    private String Fname;
    private String Lname;
    private int Age;
    private String Gender;
    private String Phone;
    private String Address;
    private String Rank;
    private float WagesPerHour;
    private String Email;
    private String Type;
    private float Salary;
    private String IncomeType;
    private String Password;
    private int BrcCode;

    public Employee(int Code, String Fname, String Lname, int Age, String Gender, String Phone, String Address, String Rank, float WagesPerHour, String Email, String Type, float Salary, String IncomeType, String Password, int BrcCode) {
        this.Code = Code;
        this.Fname = Fname;
        this.Lname = Lname;
        this.Age = Age;
        this.Gender = Gender;
        this.Phone = Phone;
        this.Address = Address;
        this.Rank = Rank;
        this.WagesPerHour = WagesPerHour;
        this.Email = Email;
        this.Type = Type;
        this.Salary = Salary;
        this.IncomeType = IncomeType;
        this.Password = Password;
        this.BrcCode = BrcCode;
    }

    public Employee(String Fname, String Lname, int Age, String Gender, String Phone, String Address, String Rank, float WagesPerHour, String Email, String Type, float Salary, String IncomeType, String Password, int BrcCode) {
        this.Code = -1;
        this.Fname = Fname;
        this.Lname = Lname;
        this.Age = Age;
        this.Gender = Gender;
        this.Phone = Phone;
        this.Address = Address;
        this.Rank = Rank;
        this.WagesPerHour = WagesPerHour;
        this.Email = Email;
        this.Type = Type;
        this.Salary = Salary;
        this.IncomeType = IncomeType;
        this.Password = Password;
        this.BrcCode = BrcCode;
    }

    public Employee(String Fname, String Lname,int BrcCode) {
        this.Code = -1;
        this.Fname = Fname;
        this.Lname = Lname;
        this.BrcCode = BrcCode;
    }

    public Employee() {
        this.Code = -1;
        this.Fname = "";
        this.Lname = "";
        this.Age = 0;
        this.Gender = "";
        this.Phone = "";
        this.Address = "";
        this.Rank = "";
        this.WagesPerHour = 0;
        this.Email = "";
        this.Type = "";
        this.Salary = 0;
        this.IncomeType = "";
        this.Password = "";
        this.BrcCode = 0;
    }

    public int getCode() {
        return Code;
    }

    public void setCode(int Code) {
        this.Code = Code;
    }

    public String getFname() {
        return Fname;
    }

    public void setFname(String Fname) {
        this.Fname = Fname;
    }

    public String getLname() {
        return Lname;
    }

    public void setLname(String Lname) {
        this.Lname = Lname;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int Age) {
        this.Age = Age;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String Gender) {
        this.Gender = Gender;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String Phone) {
        this.Phone = Phone;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getRank() {
        return Rank;
    }

    public void setRank(String Rank) {
        this.Rank = Rank;
    }

    public float getWagesPerHour() {
        return WagesPerHour;
    }

    public void setWagesPerHour(float WagesPerHour) {
        this.WagesPerHour = WagesPerHour;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public float getSalary() {
        return Salary;
    }

    public void setSalary(float Salary) {
        this.Salary = Salary;
    }

    public String getIncomeType() {
        return IncomeType;
    }

    public void setIncomeType(String IncomeType) {
        this.IncomeType = IncomeType;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public int getBrcCode() {
        return BrcCode;
    }

    public void setBrcCode(int BrcCode) {
        this.BrcCode = BrcCode;
    }

    @Override
    public String toString() {
        return "Employee{" + "Code=" + Code + ", Fname=" + Fname + ", Lname=" + Lname + ", Age=" + Age + ", Gender=" + Gender + ", Phone=" + Phone + ", Address=" + Address + ", Rank=" + Rank + ", WagesPerHour=" + WagesPerHour + ", Email=" + Email + ", Type=" + Type + ", Salary=" + Salary + ", IncomeType=" + IncomeType + ", Password=" + Password + ", BrcCode=" + BrcCode + '}';
    }

    public static Employee fromRS(ResultSet rs) {
        Employee employee = new Employee();
        try {
            employee.setCode(rs.getInt("EMP_CODE"));
            employee.setFname(rs.getString("EMP_FNAME"));
            employee.setLname(rs.getString("EMP_LNAME"));
            employee.setAge(rs.getInt("EMP_AGE"));
            employee.setGender(rs.getString("EMP_GENDER"));
            employee.setPhone(rs.getString("EMP_PHONE"));
            employee.setAddress(rs.getString("EMP_ADDRESS"));
            employee.setRank(rs.getString("EMP_RANK"));
            employee.setWagesPerHour(rs.getFloat("EMP_WAGES_PER_HOUR"));
            employee.setEmail(rs.getString("EMP_EMAIL"));
            employee.setType(rs.getString("EMP_TYPE"));
            employee.setSalary(rs.getFloat("EMP_SALARY"));
            employee.setIncomeType(rs.getString("EMP_INCOME_TYPE"));
            employee.setPassword(rs.getString("EMP_Password"));
            employee.setBrcCode(rs.getInt("BRC_CODE"));
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return employee;
    }
}
