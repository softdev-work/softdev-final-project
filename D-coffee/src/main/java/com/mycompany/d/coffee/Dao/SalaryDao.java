/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.d.coffee.Dao;

import com.mycompany.d.coffee.Helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Salary;

/**
 *
 * @author peakk
 */
public class SalaryDao implements Dao<Salary>{
    @Override
    public Salary get(int id) {
        Salary salary = null;
        String sql = "SELECT * FROM PAYSALARY WHERE PS_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                salary = new Salary();
                salary.setCode(rs.getInt("PS_CODE"));
                salary.setDate(rs.getTimestamp("PS_PAYMENT_DATETIME"));
                salary.setStatus(rs.getString("PS_STATUS"));
                salary.setBase(rs.getFloat("PS_BASE_SALARY"));
                salary.setEmployee(rs.getInt("EMP_CODE"));
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return salary;
    }

    @Override
    public List<Salary> getAll() {
        ArrayList<Salary> list = new ArrayList();
        String sql = "SELECT * FROM PAYSALARY";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Salary PAYSALARY = Salary.fromRS(rs);
                list.add(PAYSALARY);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    } // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody

   
    public Salary save(Salary obj) {
        if (obj.getCode() <= -2) {
            update(obj);
        } else if (obj.getCode() == -1) {
            String sql = "INSERT INTO PAYSALARY (PS_STATUS,PS_BASE_SALARY,EMP_CODE)" + "VALUES(?,?,?)";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setString(1, obj.getStatus());
                stmt.setFloat(2, obj.getBase());
                stmt.setInt(3,obj.getEmployee());
                stmt.executeUpdate();
                int id = DatabaseHelper.getInsertedCode(stmt);
                obj.setCode(id);
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return null;
            }
            
        } else {
            String sql = "INSERT INTO PAYSALARY ( PS_CODE,PS_STATUS,PS_BASE_SALARY,EMP_CODE)" + "VALUES(?,?,?,?)";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setInt(1,obj.getCode());
               stmt.setString(2, obj.getStatus());
                stmt.setFloat(3, obj.getBase());
                stmt.setInt(4,obj.getEmployee());
                stmt.executeUpdate();
                int id = DatabaseHelper.getInsertedCode(stmt);
                obj.setCode(id);
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return null;
            }
            
        }
        return obj;
    }
        @Override
        public Salary update(Salary obj) {
        String sql = "UPDATE PAYSALARY"
                    + " SET PS_STATUS = ?, PS_BASE_SALARY =  ?,EMP_CODE = ?"
                    + " WHERE PS_CODE = ?";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setString(1, obj.getStatus());
                stmt.setFloat(2, obj.getBase());
                 stmt.setInt(3,obj.getEmployee());
                stmt.setInt(4, obj.getCode());
                int ret = stmt.executeUpdate();
                System.out.println(ret);
                return obj;
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return null;
            }
        }

        @Override
        public int delete
        (Salary obj
        
            ) {
        String sql = "DELETE FROM PAYSALARY WHERE PS_CODE=?";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setInt(1, obj.getCode());
                int ret = stmt.executeUpdate();
                return ret;

            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
            return -1;
        }

        @Override
        public List<Salary> getAll(String where, String order) {
        ArrayList<Salary> list = new ArrayList();
            String sql = "SELECT * FROM PAYSALARY where " + where + " ORDER BY" + order;
            Connection conn = DatabaseHelper.getConnect();
            try {
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql);

                while (rs.next()) {
                    Salary PAYSALARY = Salary.fromRS(rs);
                    list.add(PAYSALARY);

                }

            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
            return list;
        }

    public List<Salary> getAll(String order) {
        ArrayList<Salary> list = new ArrayList();
        String sql = "SELECT * FROM PAYSALARY  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Salary PAYSALARY = Salary.fromRS(rs);
                list.add(PAYSALARY);

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}


