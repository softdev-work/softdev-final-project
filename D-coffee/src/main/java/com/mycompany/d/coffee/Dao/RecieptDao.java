/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.d.coffee.Dao;

import com.mycompany.d.coffee.Helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Reciept;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RecieptDao implements Dao<Reciept> {
    public static void main(String[] args) {
        String dateStr = "2023-10-18";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Date date = dateFormat.parse(dateStr);
            System.out.println(date); 
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Reciept get(int id) {
        Reciept RECIEPT = null;
        String sql = "SELECT * FROM RECIEPT WHERE RECE_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                RECIEPT = Reciept.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return RECIEPT;
    }

    @Override
    public List<Reciept> getAll() {
        ArrayList<Reciept> list = new ArrayList();
        String sql = "SELECT * FROM RECIEPT";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Reciept RECIEPT = Reciept.fromRS(rs);
                list.add(RECIEPT);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Reciept> getAll(String where, String order) {
        ArrayList<Reciept> list = new ArrayList();
        String sql = "SELECT * FROM RECIEPT where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Reciept RECIEPT = Reciept.fromRS(rs);
                list.add(RECIEPT);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Reciept> getAll(String order) {
        ArrayList<Reciept> list = new ArrayList();
        String sql = "SELECT * FROM RECIEPT ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Reciept RECIEPT = Reciept.fromRS(rs);
                list.add(RECIEPT);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }


    @Override
    public Reciept save(Reciept obj) {
        if (obj.getCode() <= -2) {
            update(obj);
        } else if (obj.getCode() == -1) {
            String sql = "INSERT INTO RECIEPT (RECE_TOTAL,RECE_DISCOUNT,RECE_CHANGE,RECE_NET_PRICE,RECE_PAYMENT,EMP_CODE,CTM_CODE,BRC_CODE,PMT_CODE)"
                    + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setFloat(1, obj.getTotal());
                stmt.setFloat(2, obj.getDiscount());
                stmt.setFloat(3, obj.getChange());
                stmt.setFloat(4, obj.getNetPrice());
                stmt.setFloat(5, obj.getPayment());
                stmt.setInt(6, obj.getEmployee());
                stmt.setInt(7, obj.getCustomer());
                stmt.setInt(8, obj.getBranch());
                stmt.setInt(9, obj.getPmtCode());
                stmt.executeUpdate();
                int id = DatabaseHelper.getInsertedCode(stmt);
                obj.setCode(id);
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return null;
            }
        } else {
            String sql = "INSERT INTO RECIEPT (RECE_CODE,RECE_TOTAL,RECE_DISCOUNT,RECE_CHANGE,RECE_NET_PRICE,RECE_PAYMENT,EMP_CODE,CTM_CODE,BRC_CODE,PMT_CODE)"
                    + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setInt(1, obj.getCode());
                stmt.setFloat(2, obj.getTotal());
                stmt.setFloat(3, obj.getDiscount());
                stmt.setFloat(4, obj.getChange());
                stmt.setFloat(5, obj.getNetPrice());
                stmt.setFloat(6, obj.getPayment());
                stmt.setInt(7, obj.getEmployee());
                stmt.setInt(8, obj.getCustomer());
                stmt.setInt(9, obj.getBranch());
                stmt.setInt(10, obj.getPmtCode());
                stmt.executeUpdate();
                int id = DatabaseHelper.getInsertedCode(stmt);
                obj.setCode(id);
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return null;
            }
        }
        return obj;
    }

    @Override
    public Reciept update(Reciept obj) {
        String sql = "UPDATE RECIEPT"
                + " SET RECE_TOTAL = ?,RECE_DISCOUNT = ?,RECE_CHANGE = ?,RECE_NET_PRICE = ?,RECE_PAYMENT = ?,EMP_CODE = ?,CTM_CODE = ?,BRC_CODE = ?,PMT_CODE = ?"
                + " WHERE RECE_CODE = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getTotal());
            stmt.setFloat(2, obj.getDiscount());
            stmt.setFloat(3, obj.getChange());
            stmt.setFloat(4, obj.getNetPrice());
            stmt.setFloat(5, obj.getPayment());
            stmt.setInt(6, obj.getEmployee());
            stmt.setInt(7, obj.getCustomer());
            stmt.setInt(8, obj.getBranch()); 
            stmt.setInt(9, obj.getPmtCode()); 
            stmt.setInt(10, obj.getCode()); 
            
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Reciept obj) {
        String sql = "DELETE FROM RECIEPT WHERE RECE_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getCode());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
