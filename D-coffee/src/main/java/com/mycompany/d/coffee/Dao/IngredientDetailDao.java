/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.d.coffee.Dao;

import com.mycompany.d.coffee.Helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Ingredient;
import model.IngredientDetail;

/**
 *
 * @author werapan
 */
public class IngredientDetailDao implements Dao<IngredientDetail> {

    @Override
    public IngredientDetail get(int id) {
        IngredientDetail ingredientDetail = null;
        String sql = "SELECT * FROMINGREDIENTSDETAILS WHERE IGD_D_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ingredientDetail = ingredientDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return ingredientDetail;
    }
    
    public IngredientDetail getByCode(int code) {
        IngredientDetail customer = null;
        String sql = "SELECT * FROM INGREDIENTSDETAILS WHERE IGD_D_CODE=?;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, code);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                customer = IngredientDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return customer;
    }

    public List<IngredientDetail> getAll() {
        ArrayList<IngredientDetail> list = new ArrayList();
        String sql = "SELECT * FROM INGREDIENTSDETAILS";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                IngredientDetail recieptDetail = IngredientDetail.fromRS(rs);
                list.add(recieptDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<IngredientDetail> getAll(String where, String order) {
        ArrayList<IngredientDetail> list = new ArrayList();
        String sql = "SELECT * FROM INGREDIENTSDETAILS where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                IngredientDetail recieptDetail = IngredientDetail.fromRS(rs);
                list.add(recieptDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<IngredientDetail> getAll(String order) {
        ArrayList<IngredientDetail> list = new ArrayList();
        String sql = "SELECT * FROM INGREDIENTSDETAILS  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                IngredientDetail recieptDetail = IngredientDetail.fromRS(rs);
                list.add(recieptDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public IngredientDetail save(IngredientDetail obj) {
        if (obj.getCode() <= -2) {
            update(obj);
        } else if (obj.getCode() == -1) {
            String sql = "INSERT INTO INGREDIENTSDETAILS (IGD_D_NAME,IGD_D_QOH,IGD_EXP_PIECES,IGD_D_DAMAGE_PIECES,IGD_D_UNIT_PRICE,IGD_D_DAMAGE_VALUE,ING_CODE,CKIGD_CODE)"
                    + "VALUES( ?, ?, ?, ?,?,?, ?, ?)";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setString(1, obj.getName());
                stmt.setInt(2, obj.getQoh());
                stmt.setInt(3, obj.getExp());
                stmt.setInt(4, obj.getDp());
                stmt.setFloat(5, obj.getUnitPrice());
                stmt.setFloat(6, obj.getDamageValue());
                stmt.setInt(7, obj.getIngCode());
                stmt.setInt(8, obj.getCkigdCode());
                stmt.executeUpdate();
                int id = DatabaseHelper.getInsertedCode(stmt);
                obj.setCode(id);
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return null;
            }
        } else {
            String sql = "INSERT INTO INGREDIENTSDETAILS (IGD_D_CODE,IGD_D_NAME,IGD_D_QOH,IGD_EXP_PIECES,IGD_D_DAMAGE_PIECES,IGD_D_UNIT_PRICE,IGD_D_DAMAGE_VALUE,ING_CODE,CKIGD_CODE)"
                    + "VALUES(?, ?, ?, ?,?,?, ?, ?, ?)";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setInt(1, obj.getCode());
                stmt.setString(2, obj.getName());
                stmt.setInt(3, obj.getQoh());
                stmt.setInt(4, obj.getExp());
                stmt.setInt(5, obj.getDp());
                stmt.setFloat(6, obj.getUnitPrice());
                stmt.setFloat(7, obj.getDamageValue());
                stmt.setInt(8, obj.getIngCode());
                stmt.setInt(9, obj.getCkigdCode());
                stmt.executeUpdate();
                int id = DatabaseHelper.getInsertedCode(stmt);
                obj.setCode(id);
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return null;
            }
        }
        return obj;
    }

    @Override
    public IngredientDetail update(IngredientDetail obj) {
        String sql = "UPDATE INGREDIENTSDETAILS"
                + " SET IGD_D_NAME = ?, IGD_D_QOH = ?, IGD_EXP_PIECES = ?, IGD_D_DAMAGE_PIECES=?, IGD_D_UNIT_PRICE = ?,IGD_D_DAMAGE_VALUE = ?,ING_CODE = ?, CKIGD_CODE = ?"
                + " WHERE IGD_D_CODE = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setString(1, obj.getName());
                stmt.setInt(2, obj.getQoh());
                stmt.setInt(3, obj.getExp());
                stmt.setInt(4, obj.getDp());
                stmt.setFloat(5, obj.getUnitPrice());
                stmt.setFloat(6, obj.getDamageValue());
                stmt.setInt(7, obj.getIngCode());
                stmt.setInt(8, obj.getCkigdCode());
                stmt.setInt(9, obj.getCode());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(IngredientDetail obj) {
        String sql = "DELETE FROM INGREDIENTSDETAILS WHERE IGD_D_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getCode());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }

    
}