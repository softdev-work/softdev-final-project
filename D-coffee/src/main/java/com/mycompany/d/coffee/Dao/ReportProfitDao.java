/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.d.coffee.Dao;

import com.mycompany.d.coffee.Helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.ReportProfit;

/**
 *
 * @author peakk
 */
public class ReportProfitDao implements Dao<ReportProfit> {

    @Override
    public ReportProfit get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<ReportProfit> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ReportProfit save(ReportProfit obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ReportProfit update(ReportProfit obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(ReportProfit obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<ReportProfit> getAll(String where, String order) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public List<ReportProfit> getProfit_loss() {
        ArrayList<ReportProfit> list = new ArrayList();

        String sql = "WITH TotalOrdering AS (SELECT SUM(ORD_PRICE) AS TotalOrderingPrice  FROM ORDERING WHERE strftime('%Y', ORD_DATE) = '2023'), "
                + "TotalIncome AS (SELECT SUM(RECE_NET_PRICE) AS INCOME FROM RECIEPT WHERE strftime('%Y', RECE_DATE) = '2023'),"
                + "TotalExpenses AS (SELECT SUM(PS_BASE_SALARY) AS TotalAllExpenses FROM PAYSALARY WHERE strftime('%Y', PS_PAYMENT_DATETIME) = '2023')"
                + "SELECT((SELECT TotalAllExpenses FROM TotalExpenses) + (SELECT TotalOrderingPrice FROM TotalOrdering)) AS EXPENSES,"
                + "(SELECT INCOME FROM TotalIncome) AS INCOME,"
                + "((SELECT TotalAllExpenses FROM TotalExpenses) + (SELECT TotalOrderingPrice FROM TotalOrdering) - (SELECT INCOME FROM TotalIncome)) AS RESULT;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ReportProfit obj = ReportProfit.fromRS(rs);
                list.add(obj);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReportProfit> getProfit_loss(String begin, String end) {
        ArrayList<ReportProfit> list = new ArrayList();
       String sql = "WITH TotalOrdering AS (SELECT SUM(ORD_PRICE) AS TotalOrderingPrice  FROM ORDERING WHERE ORD_DATE BETWEEN ? AND ?), "
                + "TotalIncome AS (SELECT SUM(RECE_NET_PRICE) AS INCOME FROM RECIEPT WHERE RECE_DATE BETWEEN ? AND ?),"
                + "TotalExpenses AS (SELECT SUM(PS_BASE_SALARY) AS TotalAllExpenses FROM PAYSALARY WHERE PS_PAYMENT_DATETIME BETWEEN ? AND ?)"
                + "SELECT((SELECT TotalAllExpenses FROM TotalExpenses) + (SELECT TotalOrderingPrice FROM TotalOrdering)) AS EXPENSES,"
                + "(SELECT INCOME FROM TotalIncome) AS INCOME,"
                + "((SELECT TotalAllExpenses FROM TotalExpenses) + (SELECT TotalOrderingPrice FROM TotalOrdering) - (SELECT INCOME FROM TotalIncome)) AS RESULT;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            stmt.setString(3, begin);
            stmt.setString(4, end);
            stmt.setString(5, begin);
            stmt.setString(6, end);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ReportProfit obj = ReportProfit.fromRS(rs);
                list.add(obj);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
