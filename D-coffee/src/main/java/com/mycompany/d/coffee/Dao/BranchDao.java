/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.d.coffee.Dao;

import com.mycompany.d.coffee.Helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Branch;

/**
 *
 * @author peakk
 */
public class BranchDao implements Dao<Branch> {

    @Override
    public Branch get(int id) {
        Branch branch = null;
        String sql = "SELECT * FROM BRANCH WHERE BRC_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                branch = new Branch();
                branch.setCode(rs.getInt("BRC_CODE"));
                branch.setName(rs.getString("BRC_NAME"));
                branch.setAdress(rs.getString("BRC_ADRESS"));
                branch.setAoe(rs.getInt("BRC_AOE"));
                branch.setPhone(rs.getString("BRC_PHONE"));
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return branch;
    }

    @Override
    public List<Branch> getAll() {
        ArrayList<Branch> list = new ArrayList();
        String sql = "SELECT * FROM BRANCH";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Branch BRANCH = Branch.fromRS(rs);
                list.add(BRANCH);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    } // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody

   
    public Branch save(Branch obj) {
           if(obj.getName().equals("") || obj.getAdress().equals("") || obj.getAoe() == 0 || obj.getPhone().equals("")){
            System.out.println("Please add data");
            } 
           else if (obj.getCode() == -1) {
            String sql = "INSERT INTO BRANCH ( BRC_NAME,BRC_ADRESS,BRC_AOE,BRC_PHONE)" + "VALUES(?,?,?,?)";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setString(1, obj.getName());
                stmt.setString(2, obj.getAdress());
                stmt.setInt(3, obj.getAoe());
                stmt.setString(4, obj.getPhone());
                stmt.executeUpdate();
                int id = DatabaseHelper.getInsertedCode(stmt);
                obj.setCode(id);
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return null;
            }
            
        } else {
            String sql = "INSERT INTO BRANCH ( BRC_CODE,BRC_NAME,BRC_ADRESS,BRC_AOE,BRC_PHONE)" + "VALUES(?,?,?,?,?)";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setInt(1,obj.getCode());
                stmt.setString(2, obj.getName());
                stmt.setString(3, obj.getAdress());
                stmt.setInt(4, obj.getAoe());
                stmt.setString(5, obj.getPhone());
                stmt.executeUpdate();
                int id = DatabaseHelper.getInsertedCode(stmt);
                obj.setCode(id);
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return null;
            }
            
        }
        return obj;
    }
        @Override
        public Branch update(Branch obj) {
        String sql = "UPDATE BRANCH"
                    + " SET BRC_NAME = ?, BRC_ADRESS = ?, BRC_AOE = ?, BRC_PHONE =  ?"
                    + " WHERE BRC_CODE = ?";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setString(1, obj.getName());
                stmt.setString(2, obj.getAdress());
                stmt.setInt(3, obj.getAoe());
                stmt.setString(4, obj.getPhone());
                stmt.setInt(5, obj.getCode());
                int ret = stmt.executeUpdate();
                System.out.println(ret);
                return obj;
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return null;
            }
        }

        @Override
        public int delete
        (Branch obj
        
            ) {
        String sql = "DELETE FROM BRANCH WHERE BRC_CODE=?";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setInt(1, obj.getCode());
                int ret = stmt.executeUpdate();
                return ret;

            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
            return -1;
        }

        @Override
        public List<Branch> getAll
        (String where, String order
        
            ) {
        ArrayList<Branch> list = new ArrayList();
            String sql = "SELECT * FROM BRANCH where " + where + " ORDER BY" + order;
            Connection conn = DatabaseHelper.getConnect();
            try {
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql);

                while (rs.next()) {
                    Branch BRANCH = Branch.fromRS(rs);
                    list.add(BRANCH);

                }

            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
            return list;
        }

    

    public List<Branch> getAll(String order) {
        ArrayList<Branch> list = new ArrayList();
        String sql = "SELECT * FROM BRANCH  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Branch BRANCH = Branch.fromRS(rs);
                list.add(BRANCH);

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
