/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.d.coffee.Dao;

import com.mycompany.d.coffee.Helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Customer;

/**
 *
 * @author werapan
 */
public class CustomerDao implements Dao<Customer> {

    @Override
    public Customer get(int id) {
        Customer CUSTOMER = null;
        String sql = "SELECT * FROM CUSTOMER WHERE CTM_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                CUSTOMER = Customer.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return CUSTOMER;
    }

    public Customer getByLogin(String name) {
        Customer CUSTOMER = null;
        String sql = "SELECT * FROM CUSTOMER WHERE CTM_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                CUSTOMER = Customer.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return CUSTOMER;
    }

    @Override
    public List<Customer> getAll() {
        ArrayList<Customer> list = new ArrayList();
        String sql = "SELECT * FROM CUSTOMER";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Customer CUSTOMER = Customer.fromRS(rs);
                list.add(CUSTOMER);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Customer> getAll(String where, String order) {
        ArrayList<Customer> list = new ArrayList();
        String sql = "SELECT * FROM CUSTOMER where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Customer CUSTOMER = Customer.fromRS(rs);
                list.add(CUSTOMER);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Customer> getAll(String order) {
        ArrayList<Customer> list = new ArrayList();
        String sql = "SELECT * FROM CUSTOMER  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Customer EMPLOYEE = Customer.fromRS(rs);
                list.add(EMPLOYEE);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Customer save(Customer obj) {
        if (obj.getCode() > 0) {
            String sql = "INSERT INTO CUSTOMER (CTM_CODE, CTM_FNAME, CTM_LNAME, CTM_AGE, CTM_PHONE, CTM_ADDRESS, CTM_EMAIL, CTM_POINT)"
                    + "VALUES(?, ?, ?, ?, ?, ?, ?)";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setInt(1, obj.getCode());
                stmt.setString(2, obj.getFname());
                stmt.setString(3, obj.getLname());
                stmt.setInt(4, obj.getAge());
                stmt.setString(5, obj.getPhone());
                stmt.setString(6, obj.getAddress());
                stmt.setString(7, obj.getEmail());
                stmt.setInt(7, obj.getPonit());
                stmt.executeUpdate();
                int id = DatabaseHelper.getInsertedCode(stmt);
                obj.setCode(id);
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return null;
            }
        } else if (obj.getFname().equals("") || obj.getLname().equals("") || obj.getAge() == 0 || obj.getPhone().equals("") || obj.getAddress().equals("") || obj.getEmail().equals("") || obj.getAge() == 0) {
            System.out.println("Please add data");
        } else if (obj.getCode() == -1) {
            String sql = "INSERT INTO CUSTOMER (CTM_FNAME, CTM_LNAME, CTM_AGE, CTM_PHONE, CTM_ADDRESS, CTM_EMAIL, CTM_POINT)"
                    + "VALUES(?, ?, ?, ?, ?, ?, ?)";
//            Connection conn = DatabaseHelper.getConnect();
            try {Connection conn = DatabaseHelper.getConnect();
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setString(1, obj.getFname());
                stmt.setString(2, obj.getLname());
                stmt.setInt(3, obj.getAge());
                stmt.setString(4, obj.getPhone());
                stmt.setString(5, obj.getAddress());
                stmt.setString(6, obj.getEmail());
                stmt.setInt(7, obj.getPonit());
                stmt.executeUpdate();
                int id = DatabaseHelper.getInsertedCode(stmt);
                obj.setCode(id);
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return null;
            }
        }
        return obj;
    }

    @Override
    public Customer update(Customer obj) {
        String sql = "UPDATE CUSTOMER"
                + " SET CTM_FNAME = ?, CTM_LNAME = ?, CTM_AGE = ? , CTM_PHONE = ?, CTM_ADDRESS = ?, CTM_EMAIL = ?, CTM_POINT = ?"
                + " WHERE CTM_CODE = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getFname());
            stmt.setString(2, obj.getLname());
            stmt.setInt(3, obj.getAge());
            stmt.setString(4, obj.getPhone());
            stmt.setString(5, obj.getAddress());
            stmt.setString(6, obj.getEmail());
            stmt.setInt(7, obj.getPonit());
            stmt.setInt(8, obj.getCode());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Customer obj) {
        String sql = "DELETE FROM CUSTOMER WHERE CTM_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getCode());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
