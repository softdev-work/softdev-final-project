/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.d.coffee.Dao;

import com.mycompany.d.coffee.Helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Ingredient;

/**
 *
 * @author werapan
 */
public class IngredientDao implements Dao<Ingredient> {

    @Override
    public Ingredient get(int id) {
        Ingredient customer = null;
        String sql = "SELECT * FROM INGREDIENTS WHERE ING_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                customer = Ingredient.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return customer;
    }

    public Ingredient getByCode(int code) {
        Ingredient customer = null;
        String sql = "SELECT * FROM INGREDIENTS WHERE ING_CODE=?;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, code);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                customer = Ingredient.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return customer;
    }

    public List<Ingredient> getAll() {
        ArrayList<Ingredient> list = new ArrayList();
        String sql = "SELECT * FROM INGREDIENTS";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Ingredient customer = Ingredient.fromRS(rs);
                list.add(customer);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Ingredient> getAll(String where, String order) {
        ArrayList<Ingredient> list = new ArrayList();
        String sql = "SELECT * FROM INGREDIENTS WHERE " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Ingredient customer = Ingredient.fromRS(rs);
                list.add(customer);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Ingredient> getAll(String order) {
        ArrayList<Ingredient> list = new ArrayList();
        String sql = "SELECT * FROM INGREDIENTS  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Ingredient customer = Ingredient.fromRS(rs);
                list.add(customer);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Ingredient save(Ingredient obj) {

        if (obj.getCode() > 0) {
            String sql = "INSERT INTO INGREDIENTS (ING_CODE,ING_NAME, ING_PRICE,ING_TYPE,ING_QOH,ING_MIN)"
                    + "VALUES( ?,  ?, ?, ?,?,?)";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setInt(1, obj.getCode());
                stmt.setString(2, obj.getIngName());
                stmt.setFloat(3, obj.getIngPrice());
                stmt.setString(4, obj.getIngType());
                stmt.setInt(5, obj.getIngQoh());
                stmt.setInt(6, obj.getIngMin());
                System.out.println("add code 0");

//            System.out.println(stmt);
                stmt.executeUpdate();
                int id = DatabaseHelper.getInsertedCode(stmt);
                obj.setCode(id);
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return null;
            }
        } 
        else if(obj.getIngName().equals("") || obj.getIngPrice() == 0 || obj.getIngMin() == 0 || obj.getIngType().equals("") || obj.getIngQoh() == 0){
            System.out.println("Please add data");
            }
        else if(obj.getCode() == -1){
            String sql = "INSERT INTO INGREDIENTS (ING_NAME, ING_PRICE,ING_TYPE,ING_QOH,ING_MIN)"
                    + "VALUES( ?,  ?, ?, ?,?)";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setString(1, obj.getIngName());
                stmt.setFloat(2, obj.getIngPrice());
                stmt.setString(3, obj.getIngType());
                stmt.setInt(4, obj.getIngQoh());
                stmt.setInt(5, obj.getIngMin());
                System.out.println("add code -1");

//            System.out.println(stmt);
                stmt.executeUpdate();
                int id = DatabaseHelper.getInsertedCode(stmt);
                obj.setCode(id);
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return null;
        }
        }
        return obj;
    }

    @Override
    public Ingredient update(Ingredient obj) {
        String sql = "UPDATE INGREDIENTS"
                + " SET  ING_NAME =  ? ,ING_PRICE = ? , ING_TYPE = ?, ING_QOH = ?, ING_MIN = ? "
                + " WHERE ING_CODE =  ? ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getIngName());
            stmt.setFloat(2, obj.getIngPrice());
            stmt.setString(3, obj.getIngType());
            stmt.setInt(4, obj.getIngQoh());
            stmt.setInt(5, obj.getIngMin());
            stmt.setInt(6, obj.getCode());

//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Ingredient obj) {
        String sql = "DELETE FROM INGREDIENTS WHERE ING_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getCode());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
