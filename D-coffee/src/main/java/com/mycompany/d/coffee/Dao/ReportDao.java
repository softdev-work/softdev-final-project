/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.d.coffee.Dao;

import com.mycompany.d.coffee.Helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Product;
import model.Report;

/**
 *
 * @author peakk
 */
public class ReportDao implements Dao<Product> {

    @Override
    public Product get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<Product> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Product save(Product obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Product update(Product obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(Product obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<Product> getAll(String where, String order) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public List<Report> getProductByAmount(int limit) {
    ArrayList<Report> list = new ArrayList();
    String sql = "SELECT pd.PD_CODE, pd.PD_NAME AS PName, SUM(RCD.RC_D_PD_AMOUNT) AS PRODUCTAMOUNT " +
            "FROM RECIEPTDETAILS RCD " +
            "NATURAL JOIN PRODUCT pd " +
            "NATURAL JOIN RECIEPT RCP "+
            "WHERE strftime('%Y', RCP.RECE_DATE) = '2023'"+
            "GROUP BY pd.PD_CODE, pd.PD_NAME " +
            "ORDER BY PRODUCTAMOUNT DESC " +
            "LIMIT ?";
    Connection conn = DatabaseHelper.getConnect();
    try {
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1,limit);
        ResultSet rs = stmt.executeQuery();
        
        while (rs.next()) {
            Report obj = Report.fromRS(rs);
            list.add(obj);
        }
    } catch (SQLException ex) {
        System.out.println(ex.getMessage());
    }
    return list;
}
    public List<Report> getProductByAmount(String begin,String end,int limit) {
    ArrayList<Report> list = new ArrayList();
    String sql = "SELECT pd.PD_CODE, pd.PD_NAME AS PName, SUM(RCD.RC_D_PD_AMOUNT) AS PRODUCTAMOUNT " +
            "FROM RECIEPTDETAILS RCD " +
            "NATURAL JOIN PRODUCT pd " +
            "NATURAL JOIN RECIEPT RCP "+
            "WHERE RCP.RECE_DATE BETWEEN ? AND ?"+
            "GROUP BY pd.PD_CODE, pd.PD_NAME " +
            "ORDER BY PRODUCTAMOUNT DESC " +
            "LIMIT ?";
    Connection conn = DatabaseHelper.getConnect();
    try {
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1,begin);
        stmt.setString(2,end);
        stmt.setInt(3,limit);
        ResultSet rs = stmt.executeQuery();
        
        while (rs.next()) {
            Report obj = Report.fromRS(rs);
            list.add(obj);
        }
    } catch (SQLException ex) {
        System.out.println(ex.getMessage());
    }
    return list;
}
}