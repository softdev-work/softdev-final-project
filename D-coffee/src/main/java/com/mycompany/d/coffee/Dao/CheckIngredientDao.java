/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.d.coffee.Dao;

import com.mycompany.d.coffee.Helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import model.CheckIngredient;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CheckIngredientDao implements Dao<CheckIngredient> {

    public static void main(String[] args) {
        String dateStr = "2023-10-18";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Date date = dateFormat.parse(dateStr);
            System.out.println(date);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public CheckIngredient get(int id) {
        CheckIngredient RECIEPT = null;
        String sql = "SELECT * FROM CHECKINGREDIENTS WHERE CKIGD_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                RECIEPT = CheckIngredient.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (ParseException ex) {
            Logger.getLogger(CheckIngredientDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return RECIEPT;
    }

    @Override
    public List<CheckIngredient> getAll() {
        ArrayList<CheckIngredient> list = new ArrayList();
        String sql = "SELECT * FROM CHECKINGREDIENTS";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckIngredient RECIEPT = CheckIngredient.fromRS(rs);
                list.add(RECIEPT);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (ParseException ex) {
            Logger.getLogger(CheckIngredientDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public List<CheckIngredient> getAll(String where, String order) {
        ArrayList<CheckIngredient> list = new ArrayList();
        String sql = "SELECT * FROM CHECKINGREDIENTS where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckIngredient RECIEPT = CheckIngredient.fromRS(rs);
                list.add(RECIEPT);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (ParseException ex) {
            Logger.getLogger(CheckIngredientDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public List<CheckIngredient> getAll(String order) {
        ArrayList<CheckIngredient> list = new ArrayList();
        String sql = "SELECT * FROM CHECKINGREDIENTS ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckIngredient RECIEPT = CheckIngredient.fromRS(rs);
                list.add(RECIEPT);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (ParseException ex) {
            Logger.getLogger(CheckIngredientDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public CheckIngredient save(CheckIngredient obj) {
        if (obj.getCode() <= -2) {
            update(obj);
        } else if (obj.getCode() == -1) {
            String sql = "INSERT INTO CHECKINGREDIENTS (CKIGD_EXP,CKIGD_DAMAGE_PIECES,CKIGD_TPD,EMP_CODE)"
                    + "VALUES( ?, ?, ?, ?)";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setInt(1, obj.getExp());
                stmt.setInt(2, obj.getDp());
                stmt.setInt(3, obj.getTdp());
                stmt.setInt(4, obj.getEmployee());
                stmt.executeUpdate();
                int id = DatabaseHelper.getInsertedCode(stmt);
                obj.setCode(id);
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return null;
            }
        } else {
            String sql = "INSERT INTO CHECKINGREDIENTS (CKIGD_CODE,CKIGD_EXP,CKIGD_DAMAGE_PIECES,CKIGD_TPD,EMP_CODE)"
                    + "VALUES(?, ?, ?, ?,?)";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setInt(1, obj.getCode());
                stmt.setInt(2, obj.getExp());
                stmt.setInt(3, obj.getDp());
                stmt.setInt(4, obj.getTdp());
                stmt.setInt(5, obj.getEmployee());
                stmt.executeUpdate();
                int id = DatabaseHelper.getInsertedCode(stmt);
                obj.setCode(id);
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return null;
            }
        }
        return obj;
    }

    @Override
    public CheckIngredient update(CheckIngredient obj) {
        String sql = "UPDATE CHECKINGREDIENTS"
                + " SET CKIGD_EXP = ?, CKIGD_DAMAGE_PIECES = ?,CKIGD_TPD = ?,EMP_CODE = ?"
                + " WHERE CKIGD_CODE = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getExp());
            stmt.setInt(2, obj.getDp());
            stmt.setInt(3, obj.getTdp());
            stmt.setInt(4, obj.getEmployee());
            stmt.setInt(5, obj.getCode());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckIngredient obj) {
        String sql = "DELETE FROM CHECKINGREDIENTS WHERE CKIGD_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getCode());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
