/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.d.coffee.Dao;

import com.mycompany.d.coffee.Helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.RecieptDetails;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RecieptDetailsDao implements Dao<RecieptDetails> {
    public static void main(String[] args) {
        String dateStr = "2023-10-18";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Date date = dateFormat.parse(dateStr);
            System.out.println(date); 
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public RecieptDetails get(int id) {
        RecieptDetails RECIEPTDETAILS = null;
        String sql = "SELECT * FROM RECIEPTDETAILS WHERE RC_D_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                RECIEPTDETAILS = RecieptDetails.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return RECIEPTDETAILS;
    }

    @Override
    public List<RecieptDetails> getAll() {
        ArrayList<RecieptDetails> list = new ArrayList();
        String sql = "SELECT * FROM RECIEPTDETAILS";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                RecieptDetails RECIEPTDETAILS = RecieptDetails.fromRS(rs);
                list.add(RECIEPTDETAILS);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<RecieptDetails> getAll(String where, String order) {
        ArrayList<RecieptDetails> list = new ArrayList();
        String sql = "SELECT * FROM RECIEPTDETAILS where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                RecieptDetails RECIEPTDETAILS = RecieptDetails.fromRS(rs);
                list.add(RECIEPTDETAILS);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<RecieptDetails> getAll(String order) {
        ArrayList<RecieptDetails> list = new ArrayList();
        String sql = "SELECT * FROM RECIEPTDETAILS ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                RecieptDetails RECIEPTDETAILS = RecieptDetails.fromRS(rs);
                list.add(RECIEPTDETAILS);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }


    @Override
    public RecieptDetails save(RecieptDetails obj) {
        if (obj.getCode() <= -2) {
            update(obj);
        } else if (obj.getCode() == -1) {
            String sql = "INSERT INTO RECIEPTDETAILS (RC_D_PD_NAME,RC_D_PD_AMOUNT,RC_D_UNIT_PRICE,RC_D_TOTAL_PRICE,RC_D_DISCOUNT,RC_D_NET_PRICE,RECE_CODE,PD_CODE,PMT_D_CODE)"
                    + "VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setString(1, obj.getProductName());
                stmt.setInt(2, obj.getProductAmount());
                stmt.setFloat(3, obj.getUnitPrice());
                stmt.setFloat(4, obj.getTotalPrice());
                stmt.setFloat(5, obj.getDiscount());
                stmt.setFloat(6, obj.getNetPrice());
                stmt.setInt(7, obj.getReceiptCode());
                stmt.setInt(8, obj.getProductCode());
                stmt.setInt(9, obj.getPromotionDetialCode());
                stmt.executeUpdate();
                int id = DatabaseHelper.getInsertedCode(stmt);
                obj.setCode(id);
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return null;
            }
        } else {
            String sql = "INSERT INTO RECIEPTDETAILS (RC_D_PD_NAME,RC_D_PD_AMOUNT,RC_D_UNIT_PRICE,RC_D_TOTAL_PRICE,RC_D_DISCOUNT,RC_D_NET_PRICE,RECE_CODE,PD_CODE,PMT_D_CODE)"
                    + "VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setString(1, obj.getProductName());
                stmt.setInt(2, obj.getProductAmount());
                stmt.setFloat(3, obj.getUnitPrice());
                stmt.setFloat(4, obj.getTotalPrice());
                stmt.setFloat(5, obj.getDiscount());
                stmt.setFloat(6, obj.getNetPrice());
                stmt.setInt(7, obj.getReceiptCode());
                stmt.setInt(8, obj.getProductCode());
                stmt.setInt(9, obj.getPromotionDetialCode());
                int id = DatabaseHelper.getInsertedCode(stmt);
                obj.setCode(id);
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return null;
            }
        }
        return obj;
    }

    @Override
    public RecieptDetails update(RecieptDetails obj) {
        String sql = "UPDATE RECIEPTDETAILS"
                + " SET RC_D_PD_NAME = ?,RC_D_PD_AMOUNT = ?,RC_D_UNIT_PRICE = ?,RC_D_TOTAL_PRICE = ?,RC_D_DISCOUNT = ?,RC_D_NET_PRICE = ?,RECE_CODE = ?,PD_CODE = ?,PMT_D_CODE = ?"
                + " WHERE RC_D_CODE = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getProductName());
            stmt.setInt(2, obj.getProductAmount());
            stmt.setFloat(3, obj.getUnitPrice());
            stmt.setFloat(4, obj.getTotalPrice());
            stmt.setFloat(5, obj.getDiscount());
            stmt.setFloat(6, obj.getNetPrice());
            stmt.setInt(7, obj.getReceiptCode());
            stmt.setInt(8, obj.getProductCode());
            stmt.setInt(9, obj.getPromotionDetialCode());
            stmt.setInt(10, obj.getCode());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            System.out.println("hello");
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(RecieptDetails obj) {
        String sql = "DELETE FROM RECIEPTDETAILS WHERE RC_D_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getCode());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
