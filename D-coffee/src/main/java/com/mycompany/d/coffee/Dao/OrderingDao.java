/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.d.coffee.Dao;

import com.mycompany.d.coffee.Helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import java.util.List;
import model.Ordering;
import model.OrderingDetails;

/**
 *
 * @author iHC
 */
public class OrderingDao implements Dao<Ordering> {
    
    @Override
    public Ordering get(int id) {
        Ordering ordering = null;
        String sql = "SELECT * FROM ORDERING WHERE ORD_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ordering = Ordering.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return ordering;
    }

    public Ordering getByLogin(String name) {
        Ordering ordering = null;
        String sql = "SELECT * FROM ORDERING WHERE ORD_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ordering = Ordering.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return ordering;
    }

    @Override
    public List<Ordering> getAll() {
        ArrayList<Ordering> list = new ArrayList();
        String sql = "SELECT * FROM ORDERING";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Ordering ordering = Ordering.fromRS(rs);
                list.add(ordering);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Ordering> getAll(String where, String order) {
        ArrayList<Ordering> list = new ArrayList();
        String sql = "SELECT * FROM ORDERING where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Ordering ordering = Ordering.fromRS(rs);
                list.add(ordering);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Ordering> getAll(String order) {
        ArrayList<Ordering> list = new ArrayList();
        String sql = "SELECT * FROM ORDERING  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Ordering ordering = Ordering.fromRS(rs);
                list.add(ordering);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Ordering save(Ordering obj) {
        if(obj.getOrdCode() >0){
            String sql = "INSERT INTO ORDERING (ORD_CODE,ORD_AMOUNT, ORD_PRICE, ORD_VENDOR, ORD_DISCOUNT, EMP_CODE)"
                + "VALUES(?, ?, ?, ?, ?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getOrdCode());
            stmt.setInt(2, obj.getOrdAmount());
            stmt.setFloat(3, obj.getOrdPrice());
            stmt.setString(4, obj.getOrdVendor());
            stmt.setFloat(5, obj.getOrdDiscount());
            stmt.setInt(6, obj.getEmployee());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedCode(stmt);
            obj.setOrdCode(id);       
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        }
        else if(obj.getOrdVendor().equals("")){
            System.out.println("Please add data");
            }
        else if(obj.getOrdCode() == -1){
            String sql = "INSERT INTO ORDERING (ORD_AMOUNT, ORD_PRICE, ORD_VENDOR, ORD_DISCOUNT, EMP_CODE)"
                    + "VALUES( ?,  ?, ?, ?,?)";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setFloat(1, obj.getOrdAmount());
                stmt.setFloat(2, obj.getOrdPrice());
                stmt.setString(3, obj.getOrdVendor());
                stmt.setFloat(4, obj.getOrdDiscount());
                stmt.setFloat(5, obj.getEmployee());

//            System.out.println(stmt);
                stmt.executeUpdate();
                int id = DatabaseHelper.getInsertedCode(stmt);
                obj.setOrdCode(id);
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return null;
        }
        }
        return obj;
    }

    @Override
    public Ordering update(Ordering obj) {
        String sql = "UPDATE ORDERING"
                + " SET ORD_AMOUNT = ?, ORD_PRICE = ?, ORD_VENDOR = ?, ORD_DISCOUNT = ?, EMP_CODE = ?"
                + " WHERE ORD_CODE = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getOrdAmount());
            stmt.setFloat(2, obj.getOrdPrice());
            stmt.setString(3, obj.getOrdVendor());
            stmt.setFloat(4, obj.getOrdDiscount());
            stmt.setInt(5, obj.getEmployee());
            stmt.setInt(6, obj.getOrdCode());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Ordering obj) {
        String sql = "DELETE FROM ORDERING WHERE ORD_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getOrdCode());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
}