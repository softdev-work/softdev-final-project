/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.d.coffee.Dao;

import com.mycompany.d.coffee.Helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import model.PromotionDetails;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.PromotionDetails;

public class PromotionDetailsDao implements Dao<PromotionDetails> {

    @Override
    public PromotionDetails get(int id) {
        PromotionDetails promotionDetails = null;
        String sql = "SELECT * FROM PROMOTIONDETAILS WHERE PMT_D_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                promotionDetails = PromotionDetails.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return promotionDetails;
    }

    @Override
    public List<PromotionDetails> getAll() {
        ArrayList<PromotionDetails> list = new ArrayList();
        String sql = "SELECT * FROM PROMOTIONDETAILS";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                PromotionDetails promotionDetails = PromotionDetails.fromRS(rs);
                list.add(promotionDetails);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<PromotionDetails> getAll(String where, String order) {
        ArrayList<PromotionDetails> list = new ArrayList();
        String sql = "SELECT * FROM PROMOTIONDETAILS where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                PromotionDetails promotionDetails = PromotionDetails.fromRS(rs);
                list.add(promotionDetails);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
        return list;
    }

    public List<PromotionDetails> getAll(String order) {
        ArrayList<PromotionDetails> list = new ArrayList();
        String sql = "SELECT * FROM PROMOTIONDETAILS ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                PromotionDetails RECIEPT = PromotionDetails.fromRS(rs);
                list.add(RECIEPT);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        
        }
        return list;
    }

    @Override
    public PromotionDetails save(PromotionDetails obj) {
        if (obj.getCode() <= -2) {
            update(obj);
        } else if (obj.getCode() == -1) {
            String sql = "INSERT INTO PROMOTIONDETAILS (PMT_D_CONDITION,PMT_T_DISCOUNT,PMT_D_NAME,PMT_D_STATUS,PD_CODE,PMT_CODE)"
                    + "VALUES( ?, ?, ?, ?, ?, ?)";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setString(1, obj.getCondition());
                stmt.setFloat(2, obj.getDiscount());
                stmt.setString(3, obj.getName());
                stmt.setString(4, obj.getStatus());
                stmt.setInt(5, obj.getProductId());
                stmt.setInt(6, obj.getPromotionId());
                stmt.executeUpdate();
                int id = DatabaseHelper.getInsertedCode(stmt);
                obj.setCode(id);
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return null;
            }
        } else {
            String sql = "INSERT INTO PROMOTIONDETAILS (PMT_D_CODE,PMT_D_CONDITION,PMT_T_DISCOUNT,PMT_D_NAME,PMT_D_STATUS,PD_CODE,PMT_CODE)"
                    + "VALUES(?, ?, ?, ?, ?, ?, ?)";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setInt(1, obj.getCode());
                stmt.setString(2, obj.getCondition());
                stmt.setFloat(3, obj.getDiscount());
                stmt.setString(4, obj.getName());
                stmt.setString(5, obj.getStatus());
                stmt.setInt(6, obj.getProductId());
                stmt.setInt(7, obj.getPromotionId());
                int id = DatabaseHelper.getInsertedCode(stmt);
                obj.setCode(id);
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return null;
            }
        }
        return obj;
    }

    @Override
    public PromotionDetails update(PromotionDetails obj) {
        String sql = "UPDATE PROMOTIONDETAILS"
                + " SET PMT_D_CONDITION = ?, PMT_T_DISCOUNT = ?, PMT_D_NAME = ?, PMT_D_STATUS = ?, PD_CODE = ?, PMT_CODE = ?"
                + " WHERE PMT_D_CODE = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
             stmt.setString(1, obj.getCondition());
             stmt.setFloat(2, obj.getDiscount());
             stmt.setString(3, obj.getName());
             stmt.setString(4, obj.getStatus());
             stmt.setInt(5, obj.getProductId());
             stmt.setInt(6, obj.getPromotionId());
             stmt.setInt(7, obj.getCode());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(PromotionDetails obj) {
        String sql = "DELETE FROM PROMOTIONDETAILS WHERE PMT_D_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getCode());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
