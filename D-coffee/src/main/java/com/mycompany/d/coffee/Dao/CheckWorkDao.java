/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.d.coffee.Dao;

import com.mycompany.d.coffee.Helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import model.CheckWork;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CheckWorkDao implements Dao<CheckWork> {

    @Override
    public CheckWork get(int id) {
        CheckWork CHECKWORK = null;
        String sql = "SELECT * FROM CHECKWORK WHERE CW_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                CHECKWORK = CheckWork.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (ParseException ex) {
            Logger.getLogger(CheckWorkDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return CHECKWORK;
    }

    @Override
    public List<CheckWork> getAll() {
        ArrayList<CheckWork> list = new ArrayList();
        String sql = "SELECT * FROM CHECKWORK";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckWork CHECKWORK = CheckWork.fromRS(rs);
                list.add(CHECKWORK);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (ParseException ex) {
            Logger.getLogger(CheckWorkDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public List<CheckWork> getAll(String where, String order) {
        ArrayList<CheckWork> list = new ArrayList();
        String sql = "SELECT * FROM CHECKWORK where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckWork CHECKWORK = CheckWork.fromRS(rs);
                list.add(CHECKWORK);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (ParseException ex) {
            Logger.getLogger(CheckWorkDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public List<CheckWork> getAll(String order) {
        ArrayList<CheckWork> list = new ArrayList();
        String sql = "SELECT * FROM CHECKWORK ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckWork CHECKWORK = CheckWork.fromRS(rs);
                list.add(CHECKWORK);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (ParseException ex) {
            Logger.getLogger(CheckWorkDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public CheckWork save(CheckWork obj) {
        if (obj.getCode() <= -2) {
            update(obj);
        } else if (obj.getCode() == -1) {
            String sql = "INSERT INTO CHECKWORK (CW_TIMEIN,CW_TIMEOUT,EMP_CODE)"
                    + "VALUES( ?, ?, ?)";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setString(1, obj.getTimein());
                stmt.setString(2, obj.getTimeout());
                stmt.setInt(3, obj.getEmployee());
                stmt.executeUpdate();
                int id = DatabaseHelper.getInsertedCode(stmt);
                obj.setCode(id);
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return null;
            }
        } else {
            String sql = "INSERT INTO CHECKWORK (CW_CODE,EMP_CODE)"
                    + "VALUES(?, ?,)";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setInt(1, obj.getCode());
                stmt.setString(2, obj.getTimein());
                stmt.setString(3, obj.getTimeout());
                stmt.setInt(4, obj.getEmployee());
                int id = DatabaseHelper.getInsertedCode(stmt);
                obj.setCode(id);
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return null;
            }
        }
        return obj;
    }

    @Override
    public CheckWork update(CheckWork obj) {
        String sql = "UPDATE CHECKWORK"
                + " SET CW_TIMEIN = ?,CW_TIMEOUT = ?,EMP_CODE = ?"
                + " WHERE CW_CODE = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getTimein());
            stmt.setString(2, obj.getTimeout());
            stmt.setInt(3, obj.getEmployee());
            stmt.setInt(4, obj.getCode());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckWork obj) {
        String sql = "DELETE FROM CHECKWORK WHERE CW_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getCode());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
