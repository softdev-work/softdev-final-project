/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.d.coffee.Dao;

import com.mycompany.d.coffee.Helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import model.Ordering;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.OrderingDetails;

public class OrderingDetailsDao implements Dao<OrderingDetails> {

    @Override
    public OrderingDetails get(int id) {
        OrderingDetails orderingDetails = null;
        String sql = "SELECT * FROM ORDERDETAILS WHERE ORD_D_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                orderingDetails = OrderingDetails.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return orderingDetails;
    }

    @Override
    public List<OrderingDetails> getAll() {
        ArrayList<OrderingDetails> list = new ArrayList();
        String sql = "SELECT * FROM ORDERDETAILS";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                OrderingDetails orderingDetails = OrderingDetails.fromRS(rs);
                list.add(orderingDetails);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<OrderingDetails> getAll(String where, String order) {
        ArrayList<OrderingDetails> list = new ArrayList();
        String sql = "SELECT * FROM ORDERDETAILS where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                OrderingDetails orderingDetails = OrderingDetails.fromRS(rs);
                list.add(orderingDetails);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
        return list;
    }

    public List<OrderingDetails> getAll(String order) {
        ArrayList<OrderingDetails> list = new ArrayList();
        String sql = "SELECT * FROM ORDERDETAILS ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                OrderingDetails ORDERING = OrderingDetails.fromRS(rs);
                list.add(ORDERING);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        
        }
        return list;
    }

    @Override
    public OrderingDetails save(OrderingDetails obj) {
        if (obj.getName().equals("") || obj.getAmount() == 0 || obj.getUnitPrice() == 0) {
            System.out.println("Please add data");
        } else if (obj.getCode() == -1) {
            String sql = "INSERT INTO ORDERDETAILS (ORD_D_NAME,ORD_D_AMOUNT,ORD_D_UNIT_PRICE,ORD_D_TOTAL_PRICE,ORD_D_DISCOUNT,ORD_D_NETPRICE,ORD_Code,ING_Code)"
                    + "VALUES( ?, ?, ?, ?, ?, ?, ?, ?)";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setString(1, obj.getName());
                stmt.setInt(2, obj.getAmount());
                stmt.setFloat(3, obj.getUnitPrice());
                stmt.setFloat(4, obj.getTotalPrice());
                stmt.setFloat(5, obj.getDiscount());
                stmt.setFloat(6, obj.getNetPrice());
                stmt.setInt(7, obj.getOrdCode());
                stmt.setInt(8, obj.getIngCode());
                stmt.executeUpdate();
                int id = DatabaseHelper.getInsertedCode(stmt);
                obj.setCode(id);
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return null;
            }
        } else {
            String sql = "INSERT INTO ORDERDETAILS (ORD_D_CODE,ORD_D_NAME,ORD_D_AMOUNT,ORD_D_UNIT_PRICE,ORD_D_TOTAL_PRICE,ORD_D_DISCOUNT,ORD_D_NETPRICE,ORD_Code,ING_Code)"
                    + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setInt(1, obj.getCode());
                stmt.setString(2, obj.getName());
                stmt.setInt(3, obj.getAmount());
                stmt.setFloat(4, obj.getUnitPrice());
                stmt.setFloat(5, obj.getTotalPrice());
                stmt.setFloat(6, obj.getDiscount());
                stmt.setFloat(7, obj.getNetPrice());
                stmt.setInt(8, obj.getOrdCode());
                stmt.setInt(9, obj.getIngCode());
                int id = DatabaseHelper.getInsertedCode(stmt);
                obj.setCode(id);
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return null;
            }
        }
        return obj;
    }

    @Override
    public OrderingDetails update(OrderingDetails obj) {
        String sql = "UPDATE ORDERDETAILS"
                + " SET ORD_D_NAME = ?, ORD_D_AMOUNT = ?, ORD_D_UNIT_PRICE = ?, ORD_D_TOTAL_PRICE = ?, ORD_D_DISCOUNT = ?, ORD_D_NETPRICE = ?, ORD_Code = ?, ING_Code = ?"
                + " WHERE ORD_D_CODE = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
             stmt.setString(1, obj.getName());
                stmt.setInt(2, obj.getAmount());
                stmt.setFloat(3, obj.getUnitPrice());
                stmt.setFloat(4, obj.getTotalPrice());
                stmt.setFloat(5, obj.getDiscount());
                stmt.setFloat(6, obj.getNetPrice());
                stmt.setInt(7, obj.getOrdCode());
                stmt.setInt(8, obj.getIngCode());
             stmt.setInt(9, obj.getCode());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(OrderingDetails obj) {
        String sql = "DELETE FROM ORDERDETAILS WHERE ORD_D_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getCode());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
