/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.d.coffee;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author sTCFILMs
 */
public class Insert {
    public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:Mockup.db";
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been establish.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }

        String sql = "INSERT INTO BRANCH(BRC_CODE,BRC_NAME,BRC_ADRESS,BRC_AOE,BRC_PHONE) VALUES (?, ?, ?, ?,?)";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, 10);
            stmt.setString(2, "SVS-coffee");
            stmt.setString(3, "Samutprakan");
            stmt.setInt(4, 8);
            stmt.setString(5, "0123456789");
            int status = stmt.executeUpdate();
            
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
