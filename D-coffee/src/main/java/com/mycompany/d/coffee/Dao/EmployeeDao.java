/*
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.d.coffee.Dao;

import com.mycompany.d.coffee.Helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Employee;

/**
 *
 * @author werapan
 */
public class EmployeeDao implements Dao<Employee> {

    @Override
    public Employee get(int id) {
        Employee EMPLOYEE = null;
        String sql = "SELECT * FROM EMPLOYEE WHERE EMP_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                EMPLOYEE = Employee.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return EMPLOYEE;
    }

    public Employee getByLogin(String name) {
        Employee EMPLOYEE = null;
        String sql = "SELECT * FROM EMPLOYEE WHERE EMP_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                EMPLOYEE = Employee.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return EMPLOYEE;
    }

    @Override
    public List<Employee> getAll() {
        ArrayList<Employee> list = new ArrayList();
        String sql = "SELECT * FROM EMPLOYEE";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Employee EMPLOYEE = Employee.fromRS(rs);
                list.add(EMPLOYEE);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Employee> getAll(String where, String order) {
        ArrayList<Employee> list = new ArrayList();
        String sql = "SELECT * FROM EMPLOYEE where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Employee EMPLOYEE = Employee.fromRS(rs);
                list.add(EMPLOYEE);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Employee> getAll(String order) {
        ArrayList<Employee> list = new ArrayList();
        String sql = "SELECT * FROM EMPLOYEE  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Employee EMPLOYEE = Employee.fromRS(rs);
                list.add(EMPLOYEE);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
//    @Override
//    public Employee save(Employee obj) {
//        if (obj.getCode() > 0) {
//            String sql = "INSERT INTO EMPLOYEE (EMP_CODE, EMP_FNAME, EMP_LNAME, EMP_AGE, EMP_GENDER,EMP_PHONE,EMP_ADDRESS,"
//                    + "EMP_RANK,EMP_WAGES_PER_HOUR,EMP_EMAIL,EMP_TYPE,EMP_SALARY,EMP_INCOME_TYPE,EMP_PASSWORD,BRC_CODE)"
//                    + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
//            Connection conn = DatabaseHelper.getConnect();
//            try {
//                PreparedStatement stmt = conn.prepareStatement(sql);
//                stmt.setInt(1, obj.getCode());
//                stmt.setString(2, obj.getFname());
//                stmt.setString(3, obj.getLname());
//                stmt.setInt(4, obj.getAge());
//                stmt.setString(5, obj.getGender());
//                stmt.setString(6, obj.getPhone());
//                stmt.setString(7, obj.getAddress());
//                stmt.setString(8, obj.getRank());
//                stmt.setFloat(9, obj.getWagesPerHour());
//                stmt.setString(10, obj.getEmail());
//                stmt.setString(11, obj.getType());
//                stmt.setFloat(12, obj.getSalary());
//                stmt.setString(13, obj.getIncomeType());
//                stmt.setString(14, obj.getPassword());
//                stmt.setInt(15, obj.getBrcCode());
//                stmt.executeUpdate();
//                int id = DatabaseHelper.getInsertedCode(stmt);
//                obj.setCode(id);
//            } catch (SQLException ex) {
//                System.out.println(ex.getMessage());
//                return null;
//            }
//        } else if (obj.getFname().equals("") || obj.getLname().equals("") || obj.getAge() == 0 || obj.getGender().equals("") || obj.getPhone().equals("") || obj.getAddress().equals("") || obj.getRank().equals("") || obj.getWagesPerHour() == 0 || obj.getEmail().equals("") || obj.getType().equals("") || obj.getSalary() == 0 || obj.getIncomeType().equals("") || obj.getPassword().equals("") || obj.getBrcCode() == 0 || obj.getAge() == 0) {
//            System.out.println("Please add data");
//        } else if (obj.getCode() == -1) {
//            String sql = "INSERT INTO EMPLOYEE (EMP_FNAME, EMP_LNAME, EMP_AGE, EMP_GENDER,"
//                    + "EMP_PHONE,EMP_ADDRESS,EMP_RANK,EMP_WAGES_PER_HOUR,EMP_EMAIL,EMP_TYPE,"
//                    + "EMP_SALARY,EMP_INCOME_TYPE,EMP_PASSWORD,BRC_CODE)"
//                    + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
//            Connection conn = DatabaseHelper.getConnect();
//            try {
//                PreparedStatement stmt = conn.prepareStatement(sql);
//                stmt.setString(1, obj.getFname());
//                stmt.setString(2, obj.getLname());
//                stmt.setInt(3, obj.getAge());
//                stmt.setString(4, obj.getGender());
//                stmt.setString(5, obj.getPhone());
//                stmt.setString(6, obj.getAddress());
//                stmt.setString(7, obj.getRank());
//                stmt.setDouble(8, obj.getWagesPerHour());
//                stmt.setString(9, obj.getEmail());
//                stmt.setString(10, obj.getType());
//                stmt.setDouble(11, obj.getSalary());
//                stmt.setString(12, obj.getIncomeType());
//                stmt.setString(13, obj.getPassword());
//                stmt.setInt(14, obj.getBrcCode());
//                stmt.executeUpdate();
//                int id = DatabaseHelper.getInsertedCode(stmt);
//                obj.setCode(id);
//            } catch (SQLException ex) {
//                System.out.println(ex.getMessage());
//                return null;
//            }
//        }
//        return obj;
//    }

    @Override
    public Employee save(Employee obj) {
        if (obj.getCode() > 0) {
            String sql = "INSERT INTO EMPLOYEE (EMP_CODE, EMP_FNAME, EMP_LNAME, EMP_AGE, EMP_GENDER,EMP_PHONE,EMP_ADDRESS,EMP_RANK,EMP_WAGES_PER_HOUR,EMP_EMAIL,EMP_TYPE,EMP_SALARY,EMP_INCOME_TYPE,EMP_PASSWORD,BRC_CODE)"
                    + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setInt(1, obj.getCode());
                stmt.setString(2, obj.getFname());
                stmt.setString(3, obj.getLname());
                stmt.setInt(4, obj.getAge());
                stmt.setString(5, obj.getGender());
                stmt.setString(6, obj.getPhone());
                stmt.setString(7, obj.getAddress());
                stmt.setString(8, obj.getRank());
                stmt.setFloat(9, obj.getWagesPerHour());
                stmt.setString(10, obj.getEmail());
                stmt.setString(11, obj.getType());
                stmt.setFloat(12, obj.getSalary());
                stmt.setString(13, obj.getIncomeType());
                stmt.setString(14, obj.getPassword());
                stmt.setInt(15, obj.getBrcCode());
                stmt.executeUpdate();
                int id = DatabaseHelper.getInsertedCode(stmt);
                obj.setCode(id);
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return null;
            }
        }   
        else if(obj.getFname().equals("") || obj.getLname().equals("")|| obj.getSalary()== 0|| obj.getWagesPerHour()== 0 || obj.getAge() == 0 || obj.getPhone().equals("") || obj.getAddress().equals("") || obj.getIncomeType().equals("")  || obj.getRank().equals("") || obj.getType().equals("")|| obj.getBrcCode()== 0 || obj.getEmail().equals("")|| obj.getAge() == 0){
            System.out.println("Please add data!!!!!!!!!!!!!!!!!!");  
        }if(obj.getCode() == -1){
            String sql = "INSERT INTO EMPLOYEE (EMP_FNAME, EMP_LNAME, EMP_AGE, EMP_GENDER,EMP_PHONE,EMP_ADDRESS,EMP_RANK,EMP_WAGES_PER_HOUR,EMP_EMAIL,EMP_TYPE,EMP_SALARY,EMP_INCOME_TYPE,EMP_PASSWORD,BRC_CODE)"
                    + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setString(1, obj.getFname());
                stmt.setString(2, obj.getLname());
                stmt.setInt(3, obj.getAge());
                stmt.setString(4, obj.getGender());
                stmt.setString(5, obj.getPhone());
                stmt.setString(6, obj.getAddress());
                stmt.setString(7, obj.getRank());
                stmt.setDouble(8, obj.getWagesPerHour());
                stmt.setString(9, obj.getEmail());
                stmt.setString(10, obj.getType());
                stmt.setDouble(11, obj.getSalary());
                stmt.setString(12, obj.getIncomeType());
                stmt.setString(13, obj.getPassword());
                stmt.setInt(14, obj.getBrcCode());
//            System.out.println(stmt);
                stmt.executeUpdate();
                int id = DatabaseHelper.getInsertedCode(stmt);
                obj.setCode(id);
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return null;
            }
        }
        return obj;
    }

    @Override
    public Employee update(Employee obj) {
        String sql = "UPDATE EMPLOYEE"
                + " SET EMP_FNAME = ?, EMP_LNAME = ?, EMP_AGE = ?, EMP_GENDER = ?, EMP_PHONE = ?, EMP_ADDRESS = ?, EMP_RANK = ?,EMP_WAGES_PER_HOUR =?, EMP_EMAIL = ?, EMP_TYPE = ?,EMP_SALARY = ?, EMP_INCOME_TYPE = ?,EMP_PASSWORD = ?, BRC_CODE = ?"
                + " WHERE EMP_CODE = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getFname());
            stmt.setString(2, obj.getLname());
            stmt.setInt(3, obj.getAge());
            stmt.setString(4, obj.getGender());
            stmt.setString(5, obj.getPhone());
            stmt.setString(6, obj.getAddress());
            stmt.setString(7, obj.getRank());
            stmt.setDouble(8, obj.getWagesPerHour());
            stmt.setString(9, obj.getEmail());
            stmt.setString(10, obj.getType());
            stmt.setDouble(11, obj.getSalary());
            stmt.setString(12, obj.getIncomeType());
            stmt.setString(13, obj.getPassword());
            stmt.setInt(14, obj.getBrcCode());
            stmt.setInt(15, obj.getCode());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Employee obj) {
        String sql = "DELETE FROM EMPLOYEE WHERE EMP_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getCode());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
