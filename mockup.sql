--
-- File generated with SQLiteStudio v3.4.4 on จ. ต.ค. 9 19:08:19 2023
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: BRANCH
DROP TABLE IF EXISTS BRANCH;

CREATE TABLE IF NOT EXISTS BRANCH (
    BRC_CODE   INTEGER   PRIMARY KEY ASC AUTOINCREMENT
                         NOT NULL,
    BRC_NAME   TEXT      NOT NULL,
    BRC_ADRESS TEXT      NOT NULL,
    BRC_AOE    INTEGER   NOT NULL,
    BRC_PHONE  TEXT (10) NOT NULL
);

INSERT INTO BRANCH (
                       BRC_CODE,
                       BRC_NAME,
                       BRC_ADRESS,
                       BRC_AOE,
                       BRC_PHONE
                   )
                   VALUES (
                       1,
                       'D-coffee',
                       'Buu',
                       10,
                       '0001112222'
                   );


-- Table: CHECK WORK
DROP TABLE IF EXISTS [CHECK WORK];

CREATE TABLE IF NOT EXISTS [CHECK WORK] (
    CW_CODE          INTEGER  PRIMARY KEY ASC AUTOINCREMENT
                              NOT NULL,
    CW_DATEIN        DATETIME NOT NULL,
    CW_DATEOUT       DATETIME NOT NULL,
    CW_WORKING_HOURS INTEGER  NOT NULL,
    CW_TYPE          TEXT (2) DEFAULT NM,
    CW_STATUS        TEXT     DEFAULT normal,
    EMP_CODE                  REFERENCES EMPLOYEE (EMP_CODE) ON DELETE RESTRICT
                                                             ON UPDATE CASCADE
);


-- Table: EMPLOYEE
DROP TABLE IF EXISTS EMPLOYEE;

CREATE TABLE IF NOT EXISTS EMPLOYEE (
    EMP_CODE           INTEGER   PRIMARY KEY ASC AUTOINCREMENT
                                 NOT NULL,
    EMP_FNAME          TEXT      NOT NULL,
    EMP_LNAME          TEXT      NOT NULL,
    EMP_AGE            INTEGER   NOT NULL,
    EMP_GENDER         TEXT (1)  DEFAULT M
                                 NOT NULL,
    EMP_PHONE          TEXT (10) NOT NULL,
    EMP_ADRESS         TEXT      NOT NULL,
    EMP_RANK           TEXT      NOT NULL,
    EMP_WAGES_PER_HOUR REAL      NOT NULL,
    EMP_EMAIL          TEXT      NOT NULL,
    EMP_TYPE           TEXT      NOT NULL,
    EMP_SALARY         REAL      NOT NULL,
    EMP_INCOME_TYPE    TEXT (1)  DEFAULT M
                                 NOT NULL,
    BRC_CODE           INTEGER   NOT NULL
                                 REFERENCES BRANCH (BRC_CODE) ON DELETE RESTRICT
                                                              ON UPDATE CASCADE
);

INSERT INTO EMPLOYEE (
                         EMP_CODE,
                         EMP_FNAME,
                         EMP_LNAME,
                         EMP_AGE,
                         EMP_GENDER,
                         EMP_PHONE,
                         EMP_ADRESS,
                         EMP_RANK,
                         EMP_WAGES_PER_HOUR,
                         EMP_EMAIL,
                         EMP_TYPE,
                         EMP_SALARY,
                         EMP_INCOME_TYPE,
                         BRC_CODE
                     )
                     VALUES (
                         1,
                         'Thanapol',
                         'Chiraporn',
                         19,
                         'M',
                         '0955976647',
                         'BUU',
                         'employee',
                         42.5,
                         '65160072@gmail.com',
                         'normal',
                         50.0,
                         'M',
                         1
                     );


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
